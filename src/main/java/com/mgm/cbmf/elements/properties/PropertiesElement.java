package com.mgm.cbmf.elements.properties;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mgm.cbmf.models.StringTemplates;

/**
 * contain all properties 
 * @author bugge
 * @since 22.01.2016
 */
public class PropertiesElement {
	private final static Logger LOGGER = Logger.getLogger(PropertiesElement.class.getName());

	private String benchmarkName = null;
	private Map<String,ScenarioElement> scenarios = new HashMap<String,ScenarioElement>();
	private ZabbixPropertiesElement zabbixProperties = new ZabbixPropertiesElement();
	private Map<String,String> copyConfigFilePaths = new HashMap<String,String>(); 
	private String evaluationResultPath = null;
	private String evaluationZipResultPath = null;
	private boolean logJavaOutput = false;
	private final String propertiesPath;
	private final Date startDate = new Date();
	private DBElement db = new DBElement();
	
	public PropertiesElement(String propertiesPath){
		this.propertiesPath = propertiesPath;
	}
	
	public String getBenchmarkName() {
		return benchmarkName;
	}
	public void setBenchmarkName(String benchmarkName) {
		this.benchmarkName = benchmarkName;
	}
	public Map<String,ScenarioElement> getScenarios() {
		return scenarios;
	}
	public void setScenarios(Map<String,ScenarioElement> scenario) {
		this.scenarios = scenario;
	}
	public void addScenario(String key,ScenarioElement scenario){
		this.scenarios.put(key,scenario);
	}
	public String getEvaluationResultPath() {
		return evaluationResultPath;
	}
	public void setEvaluationResultPath(String evaluationResultPath) {
		this.evaluationResultPath = evaluationResultPath;
	}
	public ZabbixPropertiesElement getZabbixProperties() {
		return zabbixProperties;
	}
	public void setZabbixProperties(ZabbixPropertiesElement zabbixProperties) {
		this.zabbixProperties = zabbixProperties;
	}
	public boolean isLogJavaOutput() {
		return logJavaOutput;
	}
	public void setLogJavaOutput(boolean logJavaOutput) {
		this.logJavaOutput = logJavaOutput;
	}
	
	public String getPropertiesPath() {
		return propertiesPath;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public String getEvaluationZipResultPath() {
		return evaluationZipResultPath;
	}

	public void setEvaluationZipResultPath(String evaluationZipResultPath) {
		this.evaluationZipResultPath = evaluationZipResultPath;
	}

	public Map<String, String> getCopyConfigFilePaths() {
		return copyConfigFilePaths;
	}

	public void setCopyConfigFilePaths(Map<String, String> copyConfigFilePaths) {
		this.copyConfigFilePaths = copyConfigFilePaths;
	}
	
	public void addCopyConfigFilePath(String key, String path){
		this.copyConfigFilePaths.put(key,path);
	}

	public DBElement getDb() {
		return db;
	}

	public void setDb(DBElement db) {
		this.db = db;
	}

	public boolean isValid(){
		
		if(benchmarkName == null){
			LOGGER.error(StringTemplates
					.outputPropertiesElementNotValid(StringTemplates.propertiesBenchmarlNameIndikatorKey));
			return false;
		}else if(evaluationResultPath == null){
			LOGGER.error(StringTemplates
					.outputPropertiesElementNotValid(StringTemplates.propertiesEvaluationResultPathIndikatorKey));
			return false;
		}else if(evaluationZipResultPath == null){
			LOGGER.error(StringTemplates
					.outputPropertiesElementNotValid(StringTemplates.propertiesEvaluationZipResultPathIndikatorKey));
			return false;
		} else if(!zabbixProperties.isValid()){
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PropertiesElement [benchmarkName=" + benchmarkName + ", scenarios=" + scenarios + ", zabbixProperties="
				+ zabbixProperties + ", copyConfigFilePaths=" + copyConfigFilePaths + ", evaluationResultPath="
				+ evaluationResultPath + ", evaluationZipResultPath=" + evaluationZipResultPath + ", logJavaOutput="
				+ logJavaOutput + ", propertiesPath=" + propertiesPath + ", startDate=" + startDate + "]";
	}

}
