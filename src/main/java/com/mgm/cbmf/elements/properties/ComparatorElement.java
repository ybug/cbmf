package com.mgm.cbmf.elements.properties;

/**
 * is the properties class to set the compare settings
 * @author bugge
 * @since 24.02.2016
 */
public class ComparatorElement {

	private String jarPath = null;
	private String jarOptions = null;
	private Boolean enable = false;
	private Boolean logJarOutput = true;
	private String reportPath = null;
	private String referenceDatasetPath = null;
	private String commandLine = null;
	
	public String getJarPath() {
		return jarPath;
	}
	public void setJarPath(String jarPath) {
		this.jarPath = jarPath;
	}
	public Boolean getEnable() {
		return enable;
	}
	public void setEnable(Boolean enable) {
		this.enable = enable;
	}
	public Boolean getLogJarOutput() {
		return logJarOutput;
	}
	public void setLogJarOutput(Boolean logJarOutput) {
		this.logJarOutput = logJarOutput;
	}
	public String getReportPath() {
		return reportPath;
	}
	public void setReportPath(String reportPath) {
		this.reportPath = reportPath;
	}
	public String getReferenceDatasetPath() {
		return referenceDatasetPath;
	}
	public void setReferenceDatasetPath(String referenceDatasetPath) {
		this.referenceDatasetPath = referenceDatasetPath;
	}
	
	public String getJarOptions() {
		return jarOptions;
	}
	public void setJarOptions(String jarOptions) {
		this.jarOptions = jarOptions;
	}
	public boolean existReportPath(){
		if(this.reportPath == null){
			return false;
		}
		return true;
	}
	
	public boolean existReferenceDatasetPath(){
		if(this.referenceDatasetPath == null){
			return false;
		}
		return true;
	}
	public boolean isValid(){
		if(enable == true && jarPath==null && commandLine == null){
			return false;
		}
		return true;
	}
	
	public boolean existJarOptions(){
		if(this.jarOptions != null){
			return true;
		}
		return false;
	}
	
	public boolean existCommandLine(){
		return this.commandLine != null ? true : false;
	}
	
	public String getCommandLine() {
		return commandLine;
	}
	public void setCommandLine(String commandLine) {
		this.commandLine = commandLine;
	}
	
	@Override
	public String toString() {
		return "ComparatorElement [jarPath=" + jarPath + ", jarOptions=" + jarOptions + ", enable=" + enable
				+ ", logJarOutput=" + logJarOutput + ", reportPath=" + reportPath + ", referenceDatasetPath="
				+ referenceDatasetPath + ", commandLine=" + commandLine + "]";
	}
}
