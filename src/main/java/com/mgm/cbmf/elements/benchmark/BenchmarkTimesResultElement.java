package com.mgm.cbmf.elements.benchmark;

import java.util.HashMap;
import java.util.Map;

/**
 * contains TimeResultElement from all scenarios
 * @author bugge
 * @since 28.01.2016
 */
public class BenchmarkTimesResultElement {
	
	private Map<String,ScenarioTimesResultElement> scenariosTimes = new HashMap<String,ScenarioTimesResultElement>();

	public Map<String, ScenarioTimesResultElement> getScenariosTimes() {
		return scenariosTimes;
	}

	public void setScenariosTimes(Map<String, ScenarioTimesResultElement> scenariosTimes) {
		this.scenariosTimes = scenariosTimes;
	}
	
	public void addScenariosTimes(String key, ScenarioTimesResultElement scenarioTimes) {
		this.scenariosTimes.put(key, scenarioTimes);
	}

	@Override
	public String toString() {
		return "BenchmarkTimesResultElement [scenariosTimes=" + scenariosTimes + "]";
	}
}
