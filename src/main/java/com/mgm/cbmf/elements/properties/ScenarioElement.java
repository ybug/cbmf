package com.mgm.cbmf.elements.properties;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mgm.cbmf.models.DefaultValues;
import com.mgm.cbmf.models.StringTemplates;

/**
 * Contain from one scenario the instruction data
 * 
 * @author bugge
 * @since 22.01.2016
 */
public class ScenarioElement {
	private final static Logger LOGGER = Logger.getLogger(ScenarioElement.class.getName());

	private String name = null;
	private String sourceFolderPath = null;
	private Integer roundNumber = DefaultValues.roundNumber;
	private ComparatorElement compareSettings = new ComparatorElement();
	private GeneratorElement generatorSettings = new GeneratorElement();
	private CleanupElement cleanupSettings = new CleanupElement();
	private Map<String, TestCaseElement> testCases = new HashMap<String, TestCaseElement>();
	private Map<String, ScenarioDefaultValue> defaultValues = new HashMap<String, ScenarioDefaultValue>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSourceFolderPath() {
		return sourceFolderPath;
	}

	public void setSourceFolderPath(String sourceFolderPath) {
		this.sourceFolderPath = sourceFolderPath;
	}

	public Integer getRoundNumber() {
		return roundNumber;
	}

	public void setRoundNumber(Integer roundNumber) {
		this.roundNumber = roundNumber;
	}

	public void setRoundNumber(String roundNumber) {
		try {
			this.roundNumber = Integer.parseInt(roundNumber);
		} catch (NumberFormatException e) {
			this.roundNumber = DefaultValues.roundNumber;
		}

	}

	public Map<String, TestCaseElement> getTestCases() {
		return testCases;
	}

	public void setTestCases(Map<String, TestCaseElement> testCases) {
		this.testCases = testCases;
	}

	public void setInTestCaseName(String key, String name) {
		TestCaseElement testCase = this.testCases.get(key);
		if(testCase == null){
			testCase = new TestCaseElement();
		}
		testCase.setName(name);
		testCase.setCaseIndicator(key);
		this.testCases.put(key, testCase);
	}

	public void setInTestCaseJarPath(String key, String jarPath) {
		TestCaseElement testCase = this.testCases.get(key);
		if(testCase == null){
			testCase = new TestCaseElement();
		}
		testCase.setJarPath(jarPath);
		this.testCases.put(key, testCase);
	}

	public void setInTestCaseJarOptions(String key, String jarOptions) {
		TestCaseElement testCase = this.testCases.get(key);
		if(testCase == null){
			testCase = new TestCaseElement();
		}
		testCase.setJarOptions(jarOptions);
		this.testCases.put(key, testCase);
	}

	public void setInTestCaseCommandLine(String key, String commandLine) {
		TestCaseElement testCase = this.testCases.get(key);
		if(testCase == null){
			testCase = new TestCaseElement();
		}
		testCase.setCommandLine(commandLine);
		this.testCases.put(key, testCase);
	}

	public void setInTestCaseResultFolderPath(String key, String resultFolderPath) {
		TestCaseElement testCase = this.testCases.get(key);
		if(testCase == null){
			testCase = new TestCaseElement();
		}
		testCase.setResultFolderPath(resultFolderPath);
		this.testCases.put(key, testCase);
	}
	
	public void setInTestCaseBeforeCommand(String key, String beforeCommand) {
		TestCaseElement testCase = this.testCases.get(key);
		if(testCase == null){
			testCase = new TestCaseElement();
		}
		testCase.setBeforeCommand(beforeCommand);
		this.testCases.put(key, testCase);
	}
	
	public void setInTestCaseAfterCommand(String key, String afterCommand) {
		TestCaseElement testCase = this.testCases.get(key);
		if(testCase == null){
			testCase = new TestCaseElement();
		}
		testCase.setAfterCommand(afterCommand);
		this.testCases.put(key, testCase);
	}

	public Map<String, ScenarioDefaultValue> getDefaultValues() {
		return defaultValues;
	}
	
	public void setInDefaultValueName(String key, String name) {
		ScenarioDefaultValue scenarioDefaultValue = getDefaultValues().get(key);
		if(scenarioDefaultValue == null){
			scenarioDefaultValue = new ScenarioDefaultValue();
		}
		scenarioDefaultValue.setId(key);
		scenarioDefaultValue.setName(name);
		getDefaultValues().put(key, scenarioDefaultValue);
	}
	
	public void setInDefaultValueValue(String key, String value) {
		ScenarioDefaultValue scenarioDefaultValue = getDefaultValues().get(key);
		if(scenarioDefaultValue == null){
			scenarioDefaultValue = new ScenarioDefaultValue();
		}
		scenarioDefaultValue.setId(key);
		scenarioDefaultValue.setValue(value);
		getDefaultValues().put(key, scenarioDefaultValue);
	}
	
	public void setInDefaultValueUnit(String key, String unit) {
		ScenarioDefaultValue scenarioDefaultValue = getDefaultValues().get(key);
		if(scenarioDefaultValue == null){
			scenarioDefaultValue = new ScenarioDefaultValue();
		}
		scenarioDefaultValue.setId(key);
		scenarioDefaultValue.setUnit(unit);
		getDefaultValues().put(key, scenarioDefaultValue);
	}

	public void setDefaultValues(Map<String, ScenarioDefaultValue> defaultValues) {
		this.defaultValues = defaultValues;
	}

	public ComparatorElement getCompareSettings() {
		return compareSettings;
	}

	public void setCompareSettings(ComparatorElement compareSettings) {
		this.compareSettings = compareSettings;
	}

	public GeneratorElement getGeneratorSettings() {
		return generatorSettings;
	}

	public void setGeneratorSettings(GeneratorElement generatorSettings) {
		this.generatorSettings = generatorSettings;
	}

	public CleanupElement getCleanupSettings() {
		return cleanupSettings;
	}

	public void setCleanupSettings(CleanupElement cleanupSettings) {
		this.cleanupSettings = cleanupSettings;
	}

	public boolean isValid(String propertyKey) {
		if (name == null) {
			LOGGER.error(StringTemplates
					.outputScenarioElementNotValid(StringTemplates.propertiesScenarioNameIndikatorKey, propertyKey));
			return false;
		}
		if (sourceFolderPath == null) {
			LOGGER.error(StringTemplates.outputScenarioElementNotValid(
					StringTemplates.propertiesSourceFolderPathIndikatorKey, propertyKey));
			return false;
		}
		if (testCases.isEmpty()) {
			LOGGER.error(StringTemplates.outputTestCaseListisEmpty(propertyKey));
			return false;
		}
		if (!compareSettings.isValid()) {
			LOGGER.error(StringTemplates.outputScenarioElementNotValid(StringTemplates.propertiesCompareJarPathKey+" or "+StringTemplates.propertiesCompareCommandLineKey,
					propertyKey));
			return false;
		}
		if (!generatorSettings.isValid()) {
			LOGGER.error(StringTemplates.outputScenarioElementNotValid(StringTemplates.propertiesGeneratorCommandLineKey,
					propertyKey));
			return false;
		}
		if (!cleanupSettings.isValid()) {
			LOGGER.error(StringTemplates.outputScenarioElementNotValid(StringTemplates.propertiesCleanupCommandLineKey,
					propertyKey));
			return false;
		}
		if(roundNumber <= 0){
			LOGGER.error(StringTemplates.outputScenarioElementNotValid(StringTemplates.propertiesRoundIndikatorKey,
					propertyKey));
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ScenarioElement [name=" + name + ", sourceFolderPath=" + sourceFolderPath + ", roundNumber="
				+ roundNumber + ", compareSettings=" + compareSettings + ", generatorSettings=" + generatorSettings
				+ ", cleanupSettings=" + cleanupSettings + ", testCases=" + testCases + ", defaultValues="
				+ defaultValues + "]";
	}
}
