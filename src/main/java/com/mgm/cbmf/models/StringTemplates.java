package com.mgm.cbmf.models;

import java.util.Date;

import com.mgm.cbmf.elements.properties.Arguments;

/**
 * Contain all massages and Strings. 
 * @author bugge
 * @since 02.03.2016
 */
public class StringTemplates {
	
	//entries for properties 
	public static String propertiesBenchmarlNameIndikatorKey = "benchmark_name";
	public static String propertiesConfigFolderPathsIndikatorKey = "config_folder_paths";
	public static String propertiesScenarioIndikatorKey = "scenario";
	public static String propertiesScenarioNameIndikatorKey = "name";
	public static String propertiesSourceFolderPathIndikatorKey = "source_folder_path";
	public static String propertiesRoundIndikatorKey = "round_number";
	public static String propertiesTestCaseIndikatorKey = "testcase";
	public static String propertiesDefaultValueKey = "default_value";
	public static String propertiesDefaultValueNameKey = "name";
	public static String propertiesDefaultValueValueKey = "value";
	public static String propertiesDefaultValueUnitKey = "unit";
	public static String propertiesTestCaseNameIndikatorKey = "name";
	public static String propertiesTestCaseJarPathIndikatorKey = "jar_path";
	public static String propertiesTestCaseJarOptionIndikatorKey = "jar_options";
	public static String propertiesTestCaseCommandLineIndikatorKey = "command_line";
	public static String propertiesTestCaseResultFolderPathIndikatorKey = "result_folder_path";
	public static String propertiesTestCaseBeforeIndikatorKey = "command_line_before";
	public static String propertiesTestCaseAfterIndikatorKey = "command_line_after";
	public static String propertiesPropertyIndikatorKey = "common";
	public static String propertiesEvaluationResultPathIndikatorKey = "result_path";
	public static String propertiesEvaluationZipResultPathIndikatorKey = "result_zip_path";
	public static String propertiesZabbixUserNameIndikatorKey = "zabbix_username";
	public static String propertiesZabbixPasswordIndikatorKey = "zabbix_password";
	public static String propertiesZabbixUrlIndikatorKey = "zabbix_url";
	public static String propertiesZabbixHostGroupIndikatorKey = "zabbix_hostgroup";
	public static String propertiesZabbixEnabelHostsIndikatorKey = "zabbix_enableHostsAtTheStart";
	public static String propertiesZabbixDisableHostsIndikatorKey = "zabbix_disableHostsAtTheFinished";
	public static String propertiesZabbixEnableIndikatorKey = "zabbix_enable";
	
	public static String propertiesDbUrlIndikatorKey = "db_url";
	public static String propertiesDbUserIndikatorKey = "db_user";
	public static String propertiesDbPasswordIndikatorKey = "db_password";
	public static String propertiesDbJDBCDriverIndikatorKey = "db_jdbc_driver";
	public static String propertiesDbEnableIndikatorKey = "db_enable_usage";
	
	
	public static String propertiesJarLoggerKey = "log_testcase_jar_output";
	public static String propertiesCompareJarPathKey = "comparator_jar_path";
	public static String propertiesCompareJarOptionsKey = "comparator_jar_options";
	public static String propertiesCompareEnableKey = "comparator_enable";
	public static String propertiesCompareLogOutputKey = "comparator_log_output";
	public static String propertiesCompareReportPathKey = "comparator_report_path";
	public static String propertiesCompareReferenceDatasetPathKey = "comparator_reference_dataset_path";
	public static String propertiesCompareCommandLineKey = "comparator_command_line";
	
	public static String propertiesGeneratorCommandLineKey = "generator_command_line";
	public static String propertiesGeneratorLogOutputKey = "generator_log_output";
	public static String propertiesGeneratorEnableKey = "generator_enable";
	
	public static String propertiesCleanupCommandLineKey = "cleanup_command_line";
	public static String propertiesCleanupLogOutputKey = "cleanup_log_output";
	public static String propertiesCleanupEnableKey = "cleanup_enable";
	
	public static final String ARGUMENTS_CONFIG_PATH_KEY_LONG = "-config";
	public static final String ARGUMENTS_CONFIG_PATH_KEY_SHORT = "-c";
	
	
	public static String zabbixJsonTagError = "error";
	public static String zabbixJsonTagResult = "result";
	public static String zabbixJsonTagName = "name";
	public static String zabbixJsonTagDelay = "delay";
	public static String zabbixJsonTagStatus = "status";
	public static String zabbixJsonTagItemId = "itemid";
	public static String zabbixJsonTagItemIds = "itemids";
	public static String zabbixJsonTagItemGet = "item.get";
	public static String zabbixJsonTagValueType = "value_type";
	public static String zabbixJsonTagUnits = "units";
	public static String zabbixJsonTagIp = "ip";
	public static String zabbixJsonTagPort = "port";
	public static String zabbixJsonTagFilter = "filter";
	public static String zabbixJsonTagValue = "value";
	public static String zabbixJsonTagNanoSeconds = "ns";
	public static String zabbixJsonTagClock = "clock";
	public static String zabbixJsonTagHostIds = "hostids";
	public static String zabbixJsonTagHostId = "hostid";
	public static String zabbixJsonTagHostInterfaceGet = "hostinterface.get";
	public static String zabbixJsonTagHostGet = "host.get";
	public static String zabbixJsonTagHostGroupGet = "hostgroup.get";
	public static String zabbixJsonTagHostUpdate = "host.update";
	public static String zabbixJsonTagHost = "host";
	public static String zabbixJsonTagGroupIds = "groupids";
	public static String zabbixJsonTagGroupId = "groupid";
	public static String zabbixJsonTagTimeFrom = "time_from";
	public static String zabbixJsonTagTimeTill = "time_till";
	public static String zabbixJsonTagHistory = "history";
	public static String zabbixJsonTagHistoryGet = "history.get";
	public static String zabbixJsonTagTemplateId = "templateid";
	
	
	public static String outputStartBenchmarkTitle = 
			"\n++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
			"++++++++ Start Benchmark ++++++++++++++++++++++++++++++\n"+
			"+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
			new Date().toString()+"\n"+
			"+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
	
	public static String outputEndBenchmarkTitle = 
			"\n#######################################################\n"+
			"######## Finished Benchmark ###########################\n"+
			"#######################################################\n"+
			new Date().toString()+"\n"+
			"#######################################################\n";
	
	public static String outputMainPropertiesPartTitle = "--- Load Properties ---";
	public static String outputMainBenchmarkPartTitle = "--- Run Scenarios ---";
	public static String outputGetZabbixResultsPartTitle = "--- Load Zabbix Results ---";
	public static String outputWriteOutResultsPartTitle = "--- Write Out Results ---";
	
	
	public static String outputPropertiesConverterUnknownKey(String key, String value){
		return "unknown key -> "+key+" |value ->"+value+" "+StringTemplates.lookInConfig();
	}
	
	public static String outputTestCaseElementNotValid(String key, String property){
		return "test case not valid -> "+key+" |property -> "+property+" "+StringTemplates.lookInConfig();
	}
	
	public static String outputScenarioElementNotValid(String key, String property){
		return "scenario not valid | key -> "+key+" |property -> "+property+" "+StringTemplates.lookInConfig();
	}
	
	public static String outputPropertiesElementNotValid(String key){
		return "benchmark not valid | key -> "+key+" "+StringTemplates.lookInConfig();
	}
	
	public static String outputWillIgnoreThenIsSet(String ignoreKey, String usedKey,String property){
		return "Key will ignore "+ignoreKey+", then "+usedKey+"is set. |property -> "+property+" "+StringTemplates.lookInConfig();
	}
	
	public static String outputJarExecutorLogger(String message){
		return "jar message -> "+message;
	}
	
	public static String outputCommandLineExecutorLoggerInputStream(String message){
		return "command line message [input]-> "+message;
	}
	
	public static String outputCommandLineExecutorLoggerErrorStream(String message){
		return "command line message [error]-> "+message;
	}
	
	public static String outputExecutorStartTitle(String scenario, String testcase, int round){
		return "-- Start ("+scenario+"->"+testcase+"->"+(round+1)+") --";
	}
	
	public static String outputExecutorEndTitle(String scenario, String testcase, int round, String command){
		return "-- Finished ("+scenario+"->"+testcase+"->"+(round+1)+") Command: ["+command+"] --";
	}
	
	public static String outputExecutorComperatorEndTitle(String referencePath, String targetPath, boolean error){
		String statusString = "successful";
		if(error){
			statusString = "error";
		}
		return "-- Finished Compare between "+referencePath+" and "+targetPath+" -> "+statusString+" --";
	}
	
	public static String outputExecutorGeneratorEndTitle(String generateSourcePath, boolean error){
		String statusString = "successful";
		if(error){
			statusString = "error";
		}
		return "-- Finished Generator | Output path-> "+generateSourcePath+" -> "+statusString+" --";
	}
	
	public static String outputExecutorCleanUpEndTitle(String cleanupPath, boolean error){
		String statusString = "successful";
		if(error){
			statusString = "error";
		}
		return "-- Finished Cleanup :"+cleanupPath+"->"+statusString+" --";
	}
	
	public static String outputExecutorBeforeTitle(String command, String testCaseName, boolean error){
		String statusString = "successful";
		if(error){
			statusString = "error";
		}
		return "-- Finished Before in ("+testCaseName+"): "+command+"->"+statusString+" --";
	}
	
	public static String outputExecutorAfterTitle(String command, String testCaseName, boolean error){
		String statusString = "successful";
		if(error){
			statusString = "error";
		}
		return "-- Finished After in ("+testCaseName+"): "+command+"->"+statusString+" --";
	}
	
	public static String outputConnectedWithZabbix(boolean isConnected){
		if(isConnected){
			return "Successful connected with Zabbix";
		}
		return "Not connected with Zabbix";
	}
	
	public static String lookInConfig(){
		Arguments args = new Arguments();
		return "(please look in "+args.getConfigPath()+")";
	}
	
	public static String outputNoEntryFoundInZabbixResult(){
		return "Result is empty";
	}
	
	public static String outputCantEnableHosts(){
		return "Can't enable hosts";
	}
	
	public static String outputCantDisableHosts(){
		return "Can't disable hosts";
	}
	
	public static String outputCantFindHostGroup(){
		return "Can't finde host group id";
	}
	
	public static String outputCantLoginWithZabbix(){
		return "Login failed -> Zabbix server";
	}
	
	public static String outputTestCaseListisEmpty(String scenarioName){
		return "No test cases defined in properties for scenario->: "+scenarioName;
	}
	
	public static String outputCantWriteOutResultsInJsonFile(){
		return "Can't write out results in JSON file.";
	}
	
	public static String outputCantCopyFile(String source, String target){
		return "Can't copy file from "+source+" to "+target+".";
	}
	
	public static String outputCantCopyFolder(String source, String target){
		return "Can't copy folder from "+source+" to "+target+".";
	}
	
	public static String outputCantDeleteLogFile(){
		return "Can't delete log file.";
	}
	
	public static String outputCantDeleteFolder(String folderPath){
		return "Can't delete folder-> "+folderPath;
	}
	
	public static String outputFileDoesntExist(String filePath){
		return "File don't exist -> "+filePath;
	}
	
	public static String outputCantZipFolder(String folderPath, String zipFile){
		return "Can't Zip folder -> "+folderPath+" to Zip file -> "+zipFile;
	}
	
	public static String outputCantZipFile(String filePath, String zipFile){
		return "Can't Zip file -> "+filePath+" to Zip file -> "+zipFile;
	}
	
	public static String outputComparatorExitWithException(int exitCode){
		return "on the basis of a exception -> exit code: "+exitCode;
	}
	
	public static String outputComparatorExitWithResultsNotEqual(){
		return "results not equal";
	}
	
	public static String outputAND(String first, String second){
		return first+" and "+second;
	}
	
	public static String ouptutCommandLineTitleInputStream(){
		return "  --input stream--";
	}
	
	public static String ouptutCommandLineTitleErrorStream(){
		return "  --error stream--";
	}
	
	public static String outputCantConvertToJson(String type, String value){
		return " cant convert "+type+" to json: value-> "+value+" | convert to String.";
	}
	
	public static String outputDefaultValueNotValid(String id){
		return "DefaultValue -> "+id+" not valid! "+StringTemplates.lookInConfig();
	}
	
	public static String outputDBConnectionError(){
		return "Can't connect to the Database:";
	}
	
	public static String outputDBReadError(){
		return "Can't read from Database:";
	}
}
