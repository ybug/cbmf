package com.mgm.cbmf.models.executors;

import com.mgm.cbmf.models.DefaultValues;
import com.mgm.cbmf.models.StringTemplates;


public class BenchmarkCommandLineExecutor extends CommandLineExecutor {

	private String command = "";
	private String scenarioName = "";
	private String testCaseName = "";
	private String sourceFolder = "";
	private String targetFolder = "";

	private int round = -1;
	private boolean logJarOutput = false;

	/**
	 * 
	 * @param command
	 *            -> is the command to execute in the command line
	 */
	public BenchmarkCommandLineExecutor(String command) {
		this.command = command;
	}

	/**
	 * 
	 * @param scenarioName
	 *            -> is the name from the scenario
	 */
	public void setScenarioName(String scenarioName) {
		this.scenarioName = scenarioName;
	}

	/**
	 * 
	 * @param testCaseName
	 *            -> is the name from the test case
	 */
	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	/**
	 * 
	 * @param round
	 *            -> is the number of the round
	 */
	public void setRound(int round) {
		this.round = round;
	}

	/**
	 * 
	 * @param logJarOutput
	 *            -> true then log the jar output
	 */
	public void setLogJarOutput(boolean logJarOutput) {
		this.logJarOutput = logJarOutput;
	}

	/**
	 * execute the command
	 * 
	 * @param sourceFolder
	 *            -> is the folder to get the test data set
	 * @param targetFolder
	 *            -> to save the results
	 * @return true if occur the error
	 */
	public boolean runTestCaseRound(String sourceFolder, String targetFolder) {
		this.sourceFolder = sourceFolder;
		this.targetFolder = targetFolder;
		return this.run();
	}

	/**
	 * replace inputPath placeholder and output placeholder in the command with sourceFolder and targetFolder
	 * @param sourceFolder -> is the sourceFolder
	 * @param targetFolder -> is the targetFolder
	 * @return return the execute command with sourceFolder and targetFolder
	 */
	private String setInCommandLineSourceFolderAndTargetFolder(String sourceFolder, String targetFolder) {
		return command.replaceAll(DefaultValues.inputPathPlaceholders, sourceFolder)
				.replaceAll(DefaultValues.outputPathPlaceholders, targetFolder);
	}

	@Override
	public String getCommand() {
		return setInCommandLineSourceFolderAndTargetFolder(this.sourceFolder,this.targetFolder);
	}

	@Override
	public String getErrorMessage() {
		return StringTemplates.outputExecutorEndTitle(scenarioName, testCaseName, round,getCommand());
	}

	@Override
	public String getInfoMessage() {
		return StringTemplates.outputExecutorEndTitle(scenarioName, testCaseName, round,getCommand());
	}

	@Override
	public boolean isLogExecuteOutput() {
		return logJarOutput;
	}
}
