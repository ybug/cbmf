package com.mgm.cbmf.models;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.mgm.cbmf.elements.properties.ZabbixPropertiesElement;
import com.mgm.cbmf.elements.resultjson.BenchmarkResultsElement;
import com.mgm.cbmf.elements.resultjson.HostInformationElement;
import com.mgm.cbmf.elements.resultjson.HostsGroupInformationElement;
import com.mgm.cbmf.elements.resultjson.ItemInformationElement;
import com.mgm.cbmf.elements.resultjson.ItemResultElement;
import com.mgm.cbmf.elements.resultjson.ItemResultsElement;
import com.mgm.cbmf.elements.resultjson.RoundResultElement;

import io.github.hengyunabc.zabbix.api.DefaultZabbixApi;
import io.github.hengyunabc.zabbix.api.Request;
import io.github.hengyunabc.zabbix.api.RequestBuilder;

/**
 * is the Zabbix API to connect with the Zabbix server to control, and get data.
 * @author bugge
 * @since 03.02.2016
 */
public class ZabbixAPI {

	private final static Logger LOGGER = Logger.getLogger(ZabbixAPI.class.getName());
	private DefaultZabbixApi zabbixApi;
	private boolean connected = false;
	private ZabbixPropertiesElement properties;
	private boolean error = false;

	/**
	 * 
	 * @param properties -> are the properties for Zabbix
	 */
	public ZabbixAPI(ZabbixPropertiesElement properties) {
		this.properties = properties;
		if (properties.isEnable()) {
			zabbixApi = new DefaultZabbixApi(properties.getUrl());
			zabbixApi.init();
			connected = zabbixApi.login(properties.getUserName(), properties.getPassword());
		}
	}

	/**
	 * @return get the group id from all host to use in the benchmark
	 */
	public HostsGroupInformationElement getHostGroupInformationElement() {
		error = false;
		if (isConnected()) {
			return getHostGroupInformationElementByResult(getHostGroupInfomatoinFromZabbix());
		}
		error = true;
		return new HostsGroupInformationElement();
	}

	/**
	 * 
	 * @param groupID
	 *            -> is the id to the group of hosts
	 * @return get all host from the group
	 */
	public Map<Integer, HostInformationElement> getHostsByGroupID(HostsGroupInformationElement group) {
		error = false;
		if (isConnected()) {
			return getHostInformationListByResult(getHostsInformationFromZabbix(group));
		}
		error = true;
		return new HashMap<Integer, HostInformationElement>();
	}

	/**
	 * set in BenchmarkResultsElement item informations
	 * 
	 * @param results
	 */
	public void setInBenchmarkResultElementItemsInformations(BenchmarkResultsElement results) {
		error = false;
		if (!isConnected()) {
			error = true;
			return;
		}
		for (Integer hostID : results.getHostsInformation().keySet()) {
			setInBenchmarkResultElementItemsFromResults(results,
					getItemsInformationsFromZabbix(results.getHostsInformation().get(hostID)), hostID);
		}
	}

	/**
	 * get the Zabbix results for one item in this round from this host.
	 * @param round -> information about the round (start and end time stamp)
	 * @param host -> host informations
	 * @param item -> item informations
	 * @return results from this item
	 */
	public ItemResultsElement getItemResultsElementWithZabbixResults(RoundResultElement round,
			HostInformationElement host, ItemInformationElement item) {
		error = false;
		Timestamp start = round.getStartTime();
		Timestamp end = round.getEndTime();
		Integer hostID = host.getId();
		Integer itemID = item.getId();
		Integer history = item.getDataformat();
		ItemResultsElement itemResults = new ItemResultsElement();
		itemResults.setId(itemID);
		Request request = RequestBuilder.newBuilder().method(StringTemplates.zabbixJsonTagHistoryGet).paramEntry(StringTemplates.zabbixJsonTagItemIds, itemID)
				.paramEntry(StringTemplates.zabbixJsonTagHostIds, hostID).paramEntry(StringTemplates.zabbixJsonTagTimeFrom, start.getTime() / 1000L)
				.paramEntry(StringTemplates.zabbixJsonTagTimeTill, end.getTime() / 1000L).paramEntry(StringTemplates.zabbixJsonTagHistory, history).build();
		JSONObject result = zabbixApi.call(request);
		if (result.containsKey(StringTemplates.zabbixJsonTagError)) {
			LOGGER.error(result.getString(StringTemplates.zabbixJsonTagError));
			error = true;
		}else{
			itemResults.setResults(getItemResultElementListByZabbixResult(result,history));
		}
		itemResults.setJson(result.toJSONString());
		return itemResults;
	}
	
	/**
	 * enable host by hostid and return true if occur error
	 * 
	 * @param hostID
	 * @return
	 */
	public boolean enableHost(Integer hostID) {
		error = false;
		if (!isConnected()) {
			error = true;
			return true;
		}
		Request request = RequestBuilder.newBuilder().method(StringTemplates.zabbixJsonTagHostUpdate).paramEntry(StringTemplates.zabbixJsonTagHostId, hostID)
				.paramEntry(StringTemplates.zabbixJsonTagStatus, 0).build();
		JSONObject result = zabbixApi.call(request);
		if (result.containsKey(StringTemplates.zabbixJsonTagError)) {
			LOGGER.error(result.getString(StringTemplates.zabbixJsonTagError));
			error = true;
			return true;
		}
		error = true;
		return false;
	}

	/**
	 * disable host by hostid and return true if occur error
	 * 
	 * @param hostID
	 * @return
	 */
	public boolean disableHost(Integer hostID) {
		error = false;
		if (!isConnected()) {
			error = true;
			return true;
		}
		Request request = RequestBuilder.newBuilder().method(StringTemplates.zabbixJsonTagHostUpdate).paramEntry(StringTemplates.zabbixJsonTagHostId, hostID)
				.paramEntry(StringTemplates.zabbixJsonTagStatus, 1).build();
		JSONObject result = zabbixApi.call(request);
		if (result.containsKey(StringTemplates.zabbixJsonTagError)) {
			LOGGER.error(result.getString(StringTemplates.zabbixJsonTagError));
			error = true;
			return true;
		}
		error = false;
		return false;
	}
	
	/**
	 * 
	 * @return true if connected else false;
	 */
	public boolean isConnected() {
		return this.connected;
	}

	/**
	 * 
	 * @return true if connected else false;
	 */
	public boolean isConnectedWithLogOutput() {
		if (properties.isEnable()) {
			if (this.connected) {
				LOGGER.info(StringTemplates.outputConnectedWithZabbix(this.connected));
			} else {
				LOGGER.error(StringTemplates.outputConnectedWithZabbix(this.connected));
			}
		}
		return isConnected();
	}

	/**
	 * log out and destroy the connection
	 */
	public void close() {
		if (properties.isEnable() && isConnected()) {
			zabbixApi.logout();
			zabbixApi.destroy();
		}
	}

	/**
	 * 
	 * @return true is occur a error
	 */
	public boolean isError() {
		return error;
	}
	
	/**
	 * convert the Zabbix JSON result to list of ItemResultElmenets
	 * @param result -> is the JSONObject with the results from Zabbix 
	 * @param dataFormat -> is the format to convert the Zabbix value. (like String, or Number)
	 * @return converted results as list of ItemResultElements
	 */
	private List<ItemResultElement> getItemResultElementListByZabbixResult(JSONObject result,Integer dataFormat){
		List<ItemResultElement> itemResults = new ArrayList<ItemResultElement>();
		for(Object resultObject : result.getJSONArray(StringTemplates.zabbixJsonTagResult)){
			ItemResultElement itemResult = new ItemResultElement();
			JSONObject resultJSONObject = getJsonObjectFromObject(resultObject);
			itemResult.setItemID(resultJSONObject.getInteger(StringTemplates.zabbixJsonTagItemId));
			itemResult.setDataFormat(dataFormat);
			itemResult.setClock(resultJSONObject.getLong(StringTemplates.zabbixJsonTagClock));
			itemResult.setNanoseconds(resultJSONObject.getLong(StringTemplates.zabbixJsonTagNanoSeconds));
			if(itemResult.getDataFormat()==0){
				try{
					itemResult.setValueFloat(resultJSONObject.getFloat(StringTemplates.zabbixJsonTagValue));
				}catch(JSONException e){
					itemResult.setDataFormat(4); // zabbix text 
					itemResult.setValueString(resultJSONObject.getString(StringTemplates.zabbixJsonTagValue));
					LOGGER.warn(StringTemplates.outputCantConvertToJson("Float",StringTemplates.zabbixJsonTagValue), e);
				}
				
			}else if(itemResult.getDataFormat()==3){
				try{
					itemResult.setValueLong(resultJSONObject.getLong(StringTemplates.zabbixJsonTagValue));
				}catch(JSONException e){
					itemResult.setDataFormat(4); // zabbix text 
					itemResult.setValueString(resultJSONObject.getString(StringTemplates.zabbixJsonTagValue));
					LOGGER.warn(StringTemplates.outputCantConvertToJson("Long",StringTemplates.zabbixJsonTagValue), e);
				}
			}else {
				itemResult.setValueString(resultJSONObject.getString(StringTemplates.zabbixJsonTagValue));
			}
			itemResults.add(itemResult);
		}
		return itemResults;
	}

	/**
	 * 
	 * @return the host group information as JSONObject from Zabbix
	 */
	private JSONObject getHostGroupInfomatoinFromZabbix() {
		Map<String, List<String>> filter = new HashMap<String, List<String>>();
		List<String> names = new ArrayList<String>();
		names.add(properties.getHostGroup());
		filter.put("name", names);
		Request request = RequestBuilder.newBuilder().method(StringTemplates.zabbixJsonTagHostGroupGet).paramEntry(StringTemplates.zabbixJsonTagFilter, filter).build();
		return zabbixApi.call(request);
	}

	/**
	 * convert JSONObject (results from Zabbix) to HostsGroupInformationElement
	 * @param result -> is the JSON result to get from Zabbix
	 * @return the host group information from Zabbix
	 */
	private HostsGroupInformationElement getHostGroupInformationElementByResult(JSONObject result) {
		HostsGroupInformationElement hostGroup = new HostsGroupInformationElement();
		if (result.containsKey(StringTemplates.zabbixJsonTagError)) {
			LOGGER.error(result.getString(StringTemplates.zabbixJsonTagError));
			this.error = true;
			hostGroup.setId(-1);
			return hostGroup;
		}
		if (result.getJSONArray(StringTemplates.zabbixJsonTagResult).isEmpty()) {
			LOGGER.error(StringTemplates.outputNoEntryFoundInZabbixResult());
			this.error = true;
			hostGroup.setId(-1);
			return hostGroup;
		}
		hostGroup.setJson(getJsonObjectFromObject(result.getJSONArray(StringTemplates.zabbixJsonTagResult).get(0)).toJSONString());
		hostGroup.setName(getJsonObjectFromObject(result.getJSONArray(StringTemplates.zabbixJsonTagResult).get(0)).getString(StringTemplates.zabbixJsonTagName));
		hostGroup.setId(getJsonObjectFromObject(result.getJSONArray(StringTemplates.zabbixJsonTagResult).get(0)).getIntValue(StringTemplates.zabbixJsonTagGroupId));
		return hostGroup;
	}

	/**
	 * get the hosts from the host group
	 * @param group -> information about the host group
	 * @return JSONObject -> all hosts in the host group from Zabbix
	 */
	private JSONObject getHostsInformationFromZabbix(HostsGroupInformationElement group) {
		Request request = RequestBuilder.newBuilder().method(StringTemplates.zabbixJsonTagHostGet).paramEntry(StringTemplates.zabbixJsonTagGroupIds, group.getId()).build();
		return zabbixApi.call(request);
	}

	/**
	 * convert the JSONObject from Zabbix to map of Host informations
	 * @param result -> is the result JSONObject from getHostsInformationFromZabbix()
	 * @return map -> key is host id and value is the host informations
	 */
	private Map<Integer, HostInformationElement> getHostInformationListByResult(JSONObject result) {
		Map<Integer, HostInformationElement> hosts = new HashMap<Integer, HostInformationElement>();
		if (result.containsKey(StringTemplates.zabbixJsonTagError)) {
			LOGGER.error(result.getString(StringTemplates.zabbixJsonTagError));
			this.error = true;
			return hosts;
		}

		for (Object hostEntryObject : result.getJSONArray(StringTemplates.zabbixJsonTagResult)) {
			JSONObject hostEntry = getJsonObjectFromObject(hostEntryObject);
			HostInformationElement hostInformationElement = new HostInformationElement();
			hostInformationElement.setEnable(false);
			if (hostEntry.getInteger(StringTemplates.zabbixJsonTagStatus) == 0) {
				hostInformationElement.setEnable(true);
			}
			hostInformationElement.setId(hostEntry.getIntValue(StringTemplates.zabbixJsonTagHostId));
			hostInformationElement.setName(hostEntry.getString(StringTemplates.zabbixJsonTagName));
			hostInformationElement.setJsonHostGet(hostEntry.toJSONString());
			hostInformationElement.setHost(hostEntry.getString(StringTemplates.zabbixJsonTagHost));
			hosts.put(hostInformationElement.getId(),
					setInHostGroupInformationTheHardwareInformation(hostInformationElement));
		}
		return hosts;
	}

	/**
	 * extend the host information element with the hardware information
	 * @param hostInformationElement -> is the host information element without hardware informations
	 * @return modified host information element with hardware informations 
	 */
	private HostInformationElement setInHostGroupInformationTheHardwareInformation(
			HostInformationElement hostInformationElement) {
		return setInHostGroupInformationHardwareInformationByResult(hostInformationElement,
				getHostHardwareInformationFromZabbix(hostInformationElement));
	}

	/**
	 * get from Zabbix the hardware information from the HostInformationElement
	 * @param hostInformationElement -> contains information about the host to get the hardware informations
	 * @return JSONObject -> are the hardware information from Zabbix
	 */
	private JSONObject getHostHardwareInformationFromZabbix(HostInformationElement hostInformationElement) {
		Request request = RequestBuilder.newBuilder().method(StringTemplates.zabbixJsonTagHostInterfaceGet)
				.paramEntry(StringTemplates.zabbixJsonTagHostIds, hostInformationElement.getId()).build();
		return zabbixApi.call(request);
	}

	/**
	 * convert the hardware information from JSONObject and set this in hostInformationElement
	 * @param hostInformationElement -> host information without hardware informations
	 * @param result -> hardware information from Zabbix as JSONObject
	 * @return modified host information element with hardware informations 
	 */
	private HostInformationElement setInHostGroupInformationHardwareInformationByResult(
			HostInformationElement hostInformationElement, JSONObject result) {

		if (result.containsKey(StringTemplates.zabbixJsonTagError)) {
			LOGGER.error(result.getString(StringTemplates.zabbixJsonTagError));
			this.error = true;
			return hostInformationElement;
		}
		if (result.getJSONArray(StringTemplates.zabbixJsonTagResult).isEmpty()) {
			LOGGER.error(StringTemplates.outputNoEntryFoundInZabbixResult());
			this.error = true;
			return hostInformationElement;
		}
		hostInformationElement.setIp(getJsonObjectFromObject(result.getJSONArray(StringTemplates.zabbixJsonTagResult).get(0)).getString(StringTemplates.zabbixJsonTagIp));
		hostInformationElement
				.setPort(getJsonObjectFromObject(result.getJSONArray(StringTemplates.zabbixJsonTagResult).get(0)).getIntValue(StringTemplates.zabbixJsonTagPort));
		hostInformationElement
				.setJsonHostinterfaceGet(getJsonObjectFromObject(result.getJSONArray(StringTemplates.zabbixJsonTagResult).get(0)).toJSONString());
		return hostInformationElement;
	}

	/**
	 * 
	 * @param hostInformation -> are the information from the host
	 * @return -> items informations from one host as JSONObject
	 */
	private JSONObject getItemsInformationsFromZabbix(HostInformationElement hostInformation) {
		Request request = RequestBuilder.newBuilder().method(StringTemplates.zabbixJsonTagItemGet)
				.paramEntry(StringTemplates.zabbixJsonTagHostIds, hostInformation.getId()).build();
		return zabbixApi.call(request);
	}
		
	/**
	 * convert the JSONObject to java class and set this in BenchmarkResultsElement
	 * @param benchmarkResults -> save the item informations in this instance
	 * @param itemInformationResults -> is the JSONObject to convert 
	 * @param hostID -> is the ID from the host
	 */
	private void setInBenchmarkResultElementItemsFromResults(BenchmarkResultsElement benchmarkResults, JSONObject itemInformationResults,
			Integer hostID) {
		if (itemInformationResults.containsKey(StringTemplates.zabbixJsonTagError)) {
			LOGGER.error(itemInformationResults.getString(StringTemplates.zabbixJsonTagError));
			this.error = true;
			return;
		}

		for (Object itemEntryObject : itemInformationResults.getJSONArray(StringTemplates.zabbixJsonTagResult)) {
			JSONObject itemEntry = getJsonObjectFromObject(itemEntryObject);
			ItemInformationElement itemInformation = new ItemInformationElement();
			itemInformation.setName(itemEntry.getString(StringTemplates.zabbixJsonTagName));
			itemInformation.setDelay(itemEntry.getIntValue(StringTemplates.zabbixJsonTagDelay));
			itemInformation.setTemplateId(itemEntry.getIntValue(StringTemplates.zabbixJsonTagTemplateId));
			itemInformation.setEnable(false);
			if (itemEntry.getIntValue(StringTemplates.zabbixJsonTagStatus) >= 0) {
				itemInformation.setEnable(true);
			}
			itemInformation.setId(itemEntry.getIntValue(StringTemplates.zabbixJsonTagItemId));
			itemInformation.setDataformat(itemEntry.getIntValue(StringTemplates.zabbixJsonTagValueType));
			itemInformation.setUnit(itemEntry.getString(StringTemplates.zabbixJsonTagUnits));
			itemInformation.setJson(itemEntry.toJSONString());
			benchmarkResults.getHostsInformation().get(hostID).addItem(itemInformation.getId());
			ItemInformationElement itemInformationElement = benchmarkResults.getItemsInformation().get(itemInformation.getId());
			if(itemInformationElement == null){
				benchmarkResults.getItemsInformation().put(itemInformation.getId(), itemInformation);
			}
		}
	}

	/**
	 * convert Object to JSONObject
	 * 
	 * @param object
	 * @return JsonArray
	 */
	private JSONObject getJsonObjectFromObject(Object object) {
		if (object instanceof JSONObject) {
			return (JSONObject) object;
		} else {
			return (JSONObject) JSON.toJSON(object);
		}
	}
}
