package com.mgm.cbmf.elements.properties;

/**
 * Save one default value for one scenario (like data set space)
 * @author bugge
 * @since 04.05.2016
 */
public class ScenarioDefaultValue {
	
	private String id = null;
	private String name = null;
	private String value = null;
	private String unit = null;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public boolean isValid(){
		if(id == null){
			return false;
		}else if(name == null){
			return false;
		}else if(value == null){
			return false;
		}else if(unit == null){
			return false;
		}
		return true;
	}
	@Override
	public String toString() {
		return "ScenarioDefaultValue [id=" + id + ", name=" + name + ", value=" + value + ", unit=" + unit + "]";
	}
	
	
}
