#!/bin/bash

echo ""
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "~~~~~~~~~~~~~ build benchmark framework ~~~~~~~~~~~~~~"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo ""
mvn clean package

echo ""
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "~~~~~~ download ALLUXIO benchmark repository ~~~~~~~~~"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo ""
cd ../
#git clone git@bitbucket.org:ybug/cbmf-alluxiobenchmark.git
wget https://bitbucket.org/ybug/cbmf-alluxiobenchmark/get/4cb3d8154c02.zip
unzip 4cb3d8154c02.zip
mv ybug-cbmf-alluxiobenchmark-4cb3d8154c02 cbmf-alluxiobenchmark
rm 4cb3d8154c02.zip

cd cbmf-alluxiobenchmark
echo ""
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" 
echo "~~~~~~~~~~~~~~ build cbmf-alluxiobenchmark ~~~~~~~~~~~"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo ""
sh buildAll.sh
# create benchmark config files 
sh buildConfig.sh
# copy benchmark start scripts to cbmf
cp -rd tools/scripts/Flink ../cbmf/
cp -rd tools/scripts/Spark ../cbmf/
cp tools/scripts/startFlinkBenchmark.sh ../cbmf/
cp tools/scripts/startSparkBenchmark.sh ../cbmf/

cp tools/scripts/evaluateResults.sh ../cbmf/

# load generator dataset 
sh generator.sh

cd ../cbmf
chmod +x evaluateResults.sh

echo ""
echo "!!!!!! EXECUTE generate_benchmark_sources_and_ref.sh !!!!!!!"
echo ""
