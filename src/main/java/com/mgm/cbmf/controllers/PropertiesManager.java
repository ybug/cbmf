package com.mgm.cbmf.controllers;

import java.util.HashMap;
import java.util.Map;

import com.mgm.cbmf.elements.properties.PropertiesElement;
import com.mgm.cbmf.models.PropertiesConverter;
import com.mgm.cbmf.models.PropertiesOperations;

/**
 * Managed the properties and return the properties as PropertiesElement
 * 
 * @author bugge
 * @since 21.01.2016
 */
public class PropertiesManager {

	private PropertiesElement properties = null;
	private final PropertiesOperations propertiesOperations;
	private final String propertiesPath;

	/**
	 * 
	 * @param propertiesPath -> is the path to the properties file 
	 */
	public PropertiesManager(String propertiesPath) {
		this.propertiesOperations = new PropertiesOperations(propertiesPath);
		this.propertiesPath = propertiesPath;
	}

	/**
	 * load the properties file and convert this to PropertiesElement
	 * @return true if occur a error
	 */
	public boolean loadAndConvertPropertiesFromFile() {
		if(loadPropertiesFromFile()){
			return true; 
		}
		convertProperties();
		return false;
	}

	/**
	 * load from file the properties
	 * @return true if error
	 */
	private boolean loadPropertiesFromFile() {
		return this.propertiesOperations.loadProperties();
	}
	
	/**
	 * convert the loaded properties to PropertiesElement
	 */
	private void convertProperties() {
		PropertiesConverter converter = new PropertiesConverter(this.propertiesPath);
		converter.setPropertiesMap(getKeyValuesFromProperties());
		converter.convert();
		this.properties = converter.getPropertiesElement();
	}

	/**
	 * 
	 * @return all key value pairs from the properties file
	 */
	private Map<String, String> getKeyValuesFromProperties() {
		Map<String, String> keyValues = new HashMap<String, String>();
		for(Object key : this.propertiesOperations.getAllKeys()){
			keyValues.put((String) key, this.propertiesOperations.getProperty((String) key, ""));
		}
		return keyValues;
	}

	/**
	 * 
	 * @return the founded properties
	 */
	public PropertiesElement getProperties() {
		return this.properties;
	}
}
