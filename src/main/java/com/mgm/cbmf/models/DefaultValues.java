package com.mgm.cbmf.models;

/**
 * contain all default values
 * @author bugge
 * @since 27.01.2016
 */
public class DefaultValues {
	
	/**
	 * default round number for a scenario
	 */
	public static Integer roundNumber = 10;
	
	/**
	 * default enable zabbix
	 */
	public static boolean zabbixEnable = true;
	
	/**
	 * default enable zabbix hosts at the start
	 */
	public static boolean zabbixEnableHostAtTheStart=true;
	
	/**
	 * default don't disable the hosts at the end 
	 */
	public static boolean zabbixDisableHostAtTheFinished=false;
	
	/**
	 * default file name from the result set as json
	 */
	public static String resultJsonFileName = "result.json";
	
	/**
	 * default file name from the properties file
	 */
	public static String propertiesFileName = "config.properties";
	
	/**
	 * default folder path to the properties file
	 */
	public static String propertiesFolderPath = "src/main/resources/";
	
	/**
	 * default file name from the log file
	 */
	public static String logFileName = "log.log";
	
	/**
	 * default options for the test case jars
	 */
	public static String testCaseOptions = "";
	
	/**
	 * default options for the comparator jars
	 */
	public static String comparatorOptions = "";
	
	/**
	 * default compressions level 
	 */
	public static int zipCompressionLevel = 9;
	
	/**
	 * is the default exit code for not equals (windows) -> http://www.symantec.com/connect/articles/windows-system-error-codes-exit-codes-description
	 */
	public static int comparatorExitCodeNotEquals = 8229;
	
	/**
	 * is the placeholder in the command line for the inputPath (execute and measure)
	 */
	public static String inputPathPlaceholders = "<inputPath>";
	
	/**
	 * is the placeholder in the command line for the outputPath (execute and measure) 
	 */
	public static String outputPathPlaceholders = "<outputPath>";
	
	/**
	 * is the placeholder (comparator command line)
	 */
	public static String firstResultPlaceholders = "<isFirstResult>";
	
	/**
	 * is the placeholder (comparator command line)
	 */
	public static String referencePathPlaceholders = "<refPath>";
	
	/**
	 * is the placeholder (comparator command line)
	 */
	public static String resultPathPlaceholders = "<resultPath>";
	
	/**
	 * is the placeholder (comparator command line)
	 */
	public static String reportPathPlaceholders = "<reportPath>";
	
	/**
	 * is the placeholder (generator command line)
	 */
	public static String generateSourcePathPlaceholders = "<generateSourcePath>";
	
	/**
	 * is the placeholder (cleanup command line)
	 */
	public static String cleanUpFolderPathPlaceholders = "<cleanupPath>";
	
	public static String resultConfigFolderName = "used_configs";
	
	public static String resultFolderDateFormat = "yyyy_MM_dd_HH_mm_ss";
}
