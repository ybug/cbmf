package com.mgm.cbmf.models.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.mgm.cbmf.elements.resultjson.ItemResultElement;
import com.mgm.cbmf.elements.resultjson.ItemResultsElement;

public class ZabbixDBHistoryTableOperations extends DBOperations{

	private final String SQL_READ_HISTORY = "SELECT * FROM %s WHERE itemid = %d AND clock >= %d AND clock <= %d;";
	private final String HISTORY_DOUBLE_TABLENAME = "history";
	private final String HISTORY_LONG_TABLENAME = "history_uint";
	private final String HISTORY_STRING_TABLENAME = "history_str";
	
	private final String HISTORY_COLUMNNAME_ITEMID = "itemid";
	private final String HISTORY_COLUMNNAME_CLOCK = "clock";
	private final String HISTORY_COLUMNNAME_VALUE = "value";
	private final String HISTORY_COLUMNNAME_NS = "ns";
	
	private final int DATATYPE_DOUBLE = 0;
	private final int DATATYPE_LONG = 3;
	private final int DATATYPE_OTHER = -1;
	
	private final static Logger LOGGER = Logger.getLogger(ZabbixDBHistoryTableOperations.class.getName());
	
	private ItemResultsElement results =  new ItemResultsElement();
	
	/**
	 * 
	 * @param dbUrl
	 * @param user
	 * @param password
	 */
	public ZabbixDBHistoryTableOperations(String dbUrl, String user, String password) {
		super(dbUrl, user, password);
		open();
	}
	
	public void readHistory(int dataType, int itemId, long startTimeStamp, long endTimeStamp ){
		
		results =  new ItemResultsElement();
		
		if(dataType == DATATYPE_DOUBLE){
			results = readDoubleHistory(itemId,startTimeStamp,endTimeStamp);
		}else if(dataType == DATATYPE_LONG){
			results = readLongHistory(itemId,startTimeStamp,endTimeStamp);
		}else{
			results = readStringHistory(itemId,startTimeStamp,endTimeStamp);
		}
	}
	
	private ItemResultsElement readDoubleHistory(int itemId, long startTimeStamp, long endTimeStamp ){
		String sql = String.format(SQL_READ_HISTORY, HISTORY_DOUBLE_TABLENAME,itemId, startTimeStamp, endTimeStamp);
		ResultSet resultSet = this.execute(sql);
		
		ItemResultsElement results = historyResultExtractor(resultSet,DATATYPE_DOUBLE,itemId);
		this.closeResultSet(resultSet);
		return results;
	}
	
	private ItemResultsElement readLongHistory(int itemId, long startTimeStamp, long endTimeStamp ){
		String sql = String.format(SQL_READ_HISTORY, HISTORY_LONG_TABLENAME,itemId, startTimeStamp, endTimeStamp);
		ResultSet resultSet = this.execute(sql);
		
		ItemResultsElement results = historyResultExtractor(resultSet,DATATYPE_LONG,itemId);
		this.closeResultSet(resultSet);
		return results;
	}
	
	private ItemResultsElement readStringHistory(int itemId, long startTimeStamp, long endTimeStamp ){
		String sql = String.format(SQL_READ_HISTORY, HISTORY_STRING_TABLENAME,itemId, startTimeStamp, endTimeStamp);
		ResultSet resultSet = this.execute(sql);
		
		ItemResultsElement results = historyResultExtractor(resultSet,DATATYPE_OTHER,itemId);
		this.closeResultSet(resultSet);
		return results;
	}
	
	
	private ItemResultsElement historyResultExtractor(ResultSet resultSet, int dataType,int itemId){
		ItemResultsElement itemResultsElement = new ItemResultsElement();
		itemResultsElement.setId(itemId);
		List<ItemResultElement> itemResults = new ArrayList<ItemResultElement>();
		try {
			while(resultSet.next()){
				ItemResultElement itemResult = new ItemResultElement();
				itemResult.setItemID(resultSet.getInt(HISTORY_COLUMNNAME_ITEMID));
				itemResult.setDataFormat(dataType);
				itemResult.setClock(resultSet.getLong(HISTORY_COLUMNNAME_CLOCK));
				itemResult.setNanoseconds(resultSet.getLong(HISTORY_COLUMNNAME_NS));
				setInItemResultElementValue(resultSet, itemResult);
				
				itemResults.add(itemResult);
			}
		} catch (SQLException e) {
			LOGGER.error(e);
		}
		itemResultsElement.setResults(itemResults);
		itemResultsElement.setJson("");
		return itemResultsElement;
	}

	private void setInItemResultElementValue(ResultSet resultSet, ItemResultElement itemResult) throws SQLException {
		if(itemResult.getDataFormat()==0){
			try{
				itemResult.setValueFloat(resultSet.getFloat(HISTORY_COLUMNNAME_VALUE));
			}catch(Exception e){
				itemResult.setDataFormat(4);
				itemResult.setValueString(resultSet.getString(HISTORY_COLUMNNAME_VALUE));
			}
			
		}else if(itemResult.getDataFormat()==3){
			try{
				itemResult.setValueLong(resultSet.getLong(HISTORY_COLUMNNAME_VALUE));
			}catch(Exception e){
				itemResult.setDataFormat(4);
				itemResult.setValueString(resultSet.getString(HISTORY_COLUMNNAME_VALUE));
			}
		}else {
			itemResult.setValueString(resultSet.getString(HISTORY_COLUMNNAME_VALUE));
		}
	}

	public ItemResultsElement getResults() {
		if(results.getResults().isEmpty()){
			return null;
		}
		return results;
	}
}
