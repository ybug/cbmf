package com.mgm.cbmf.models.executors;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.log4j.Logger;

public abstract class JarExecutor {
	
	private final static Logger LOGGER = Logger.getLogger(JarExecutor.class.getName());
	
	private Timestamp startTimeStamp = null;
	private Timestamp endTimeStamp = null;
	
	public abstract String[] getCommand();
	public abstract String getErrorMessage();
	public abstract String getInfoMessage();
	public abstract boolean isLogJarOutput();
	
	public JarExecutor(){
		
	}
	
	public boolean run(){
		ProcessBuilder pb = new ProcessBuilder(getCommand());
		try {
			startTimeStamp = new Timestamp(new Date().getTime());
			Process process = pb.start();
			if (isLogJarOutput()) {
				logOutputFromJar(process);
			}
			process.waitFor();
			endTimeStamp = new Timestamp(new Date().getTime());
			if (process.exitValue() != 0) {
				LOGGER.error(getErrorMessage());
				return true;
			}
		} catch (IOException | InterruptedException e) {
			LOGGER.error(e.getMessage(), e);
			LOGGER.error(getErrorMessage());
			return true;
		}
		LOGGER.info(getInfoMessage());
		return false;
	}
	
	/**
	 * log the info and error stream from the executable jar
	 * 
	 * @param process
	 *            -> is the jar process
	 */
	private void logOutputFromJar(Process process) {
		JarExecutorLoggerInfo infoLogger = new JarExecutorLoggerInfo(process.getInputStream());
		Thread infoLoggerThread = new Thread(infoLogger, "JarExecutorLoggerInfo");
		infoLoggerThread.start();
		JarExecutorLoggerError errorLogger = new JarExecutorLoggerError(process.getErrorStream());
		Thread errorLoggerThread = new Thread(errorLogger, "JarExecutorLoggerError");
		errorLoggerThread.start();
	}

	
	public Timestamp getStartTimeStamp() {
		return startTimeStamp;
	}
	public Timestamp getEndTimeStamp() {
		return endTimeStamp;
	}
}
