package com.mgm.cbmf.models;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;

import com.mgm.cbmf.elements.properties.PropertiesElement;

/**
 * create Zip folder from the Results
 * 
 * @author bugge
 * @since 03.03.2016
 */
public class ZipOperations {

	private final static Logger LOGGER = Logger.getLogger(ZipOperations.class.getName());

	private PropertiesElement properties;

	/**
	 * zip the result folder
	 * 
	 * @param properties
	 *            -> all properties from this benchmark
	 */
	public static void zipResultFolder(PropertiesElement properties) {
		if (properties.isValid()) {
			ZipOperations zipOperations = new ZipOperations(properties);
			zipOperations.zipResultFolder();
		}
	}

	/**
	 * 
	 * @param properties
	 *            -> all properties from this benchmark
	 */
	public ZipOperations(PropertiesElement properties) {
		this.properties = properties;
	}

	/**
	 * Zip the result folder
	 */
	public void zipResultFolder() {
		createZipFolderIfNotExist();
		try {
			FileOutputStream fout = new FileOutputStream(getZipFilePath());
			ZipOutputStream zout = new ZipOutputStream(fout);
			zout.setLevel(DefaultValues.zipCompressionLevel);
			addFolder(zout, getSourceResultFolderPath(), getSourceResultFolderPath());
			zout.close();

		} catch (IOException ioe) {
			LOGGER.error(StringTemplates.outputCantZipFolder(getSourceResultFolderPath(), getZipFilePath()), ioe);
		}
	}

	/**
	 * add the folder to the zip archives 
	 * @param zout -> is the zip output stream
	 * @param folderPath -> is the folder path to include in the zip file
	 * @param baseFolderPath -> is the root folder path
	 */
	private void addFolder(ZipOutputStream zout, String folderPath, String baseFolderPath) {
		if (FileFolderOperations.existFileOrFolder(folderPath)) {
			if (FileFolderOperations.isPathDirectory(folderPath)) {
				addDirectory(zout, folderPath, baseFolderPath);
			} else {
				addFile(zout, folderPath, baseFolderPath);
			}
		} else {
			LOGGER.error(StringTemplates.outputFileDoesntExist(folderPath));
		}
	}

	/**
	 * add a folder to the zip archives 
	 * @param zout -> is the zip output stream
	 * @param folderPath -> is the folder path to include in the zip file
	 * @param baseFolderPath -> is the root folder path
	 */
	private void addDirectory(ZipOutputStream zout, String folderPath, String baseFolderPath) {
		if (!folderPath.equalsIgnoreCase(baseFolderPath)) {
			String entryName = folderPath.substring(baseFolderPath.length() + 1, folderPath.length())
					+ File.separatorChar;
			ZipEntry ze = new ZipEntry(entryName);
			try {
				zout.putNextEntry(ze);
			} catch (IOException e) {
				LOGGER.error(StringTemplates.outputCantZipFolder(folderPath, getZipFilePath()), e);
			}
		}
		File f2[] = FileFolderOperations.getFileFromPath(folderPath).listFiles();
		for (int i = 0; i < f2.length; i++) {
			addFolder(zout, f2[i].getAbsolutePath(), baseFolderPath);
		}
	}

	/**
	 * add the file to the zip archives
	 * @param zout -> is the zip output stream
	 * @param folderPath -> is the folder path to include in the zip file
	 * @param baseFolderPath -> is the root folder path
	 */
	private void addFile(ZipOutputStream zout, String folderPath, String baseFolderPath) {
		String entryName = folderPath.substring(baseFolderPath.length() + 1, folderPath.length());
		ZipEntry ze = new ZipEntry(entryName);
		try {
			zout.putNextEntry(ze);
			FileInputStream in = new FileInputStream(folderPath);
			int len;
			byte buffer[] = new byte[1024];
			while ((len = in.read(buffer)) > 0) {
				zout.write(buffer, 0, len);
			}
			in.close();
			zout.closeEntry();
		} catch (IOException e) {
			LOGGER.error(StringTemplates.outputCantZipFile(folderPath, getZipFilePath()), e);
		}
	}
	
	/**
	 * 
	 * @return -> get the result zip file path.
	 */
	private String getZipFilePath() {
		return Paths.get(properties.getEvaluationZipResultPath(), Long.toString(properties.getStartDate().getTime())+getConfigFileAnomalyString() + ".zip")
				.toString();
	}
	
	private String getConfigFileAnomalyString(){
		String defaultFileName = DefaultValues.propertiesFileName;
		int defaultFileNamePointIndex = defaultFileName.indexOf(".");
		String defaultFileTop = defaultFileName.substring(0, defaultFileNamePointIndex);
		String defaultFileTail = defaultFileName.substring(defaultFileNamePointIndex, defaultFileName.length());
		return new File(properties.getPropertiesPath()).getName().replace(defaultFileTop, "").replace(defaultFileTail, "");
	}
	/**
	 * 
	 * @return -> get the result folder path. this folder contains all results
	 *         from the benchmark, like log file and comparator reports
	 */
	private String getSourceResultFolderPath() {
		return Paths.get(properties.getEvaluationResultPath()).toAbsolutePath().toString();//, properties.getStartDate().toString()+getConfigFileAnomalyString()).toAbsolutePath().toString();
	}

	/**
	 * create Folder to contain the zip files
	 */
	private void createZipFolderIfNotExist() {
		String zipFolderPath = properties.getEvaluationZipResultPath();
		FileFolderOperations.createFolderIfNotExist(zipFolderPath);
	}
}
