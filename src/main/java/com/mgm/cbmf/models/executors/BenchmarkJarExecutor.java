package com.mgm.cbmf.models.executors;

import java.util.Arrays;
import com.mgm.cbmf.models.StringTemplates;

public class BenchmarkJarExecutor extends JarExecutor {
	
	private String jarPath;
	private String options = "";

	private String scenarioName = "";
	private String testCaseName = "";
	
	private String sourceFolder = "";
	private String targetFolder = "";

	private int round = -1;
	private boolean logJarOutput = false;
	

	/**
	 * 
	 * @param jarPath
	 *            -> is the path to the executable jar
	 */
	public BenchmarkJarExecutor(String jarPath) {
		super();
		this.jarPath = jarPath;
	}

	/**
	 * 
	 * @param scenarioName
	 *            -> is the name from the scenario
	 */
	public void setScenarioName(String scenarioName) {
		this.scenarioName = scenarioName;
	}

	/**
	 * 
	 * @param testCaseName
	 *            -> is the name from the test case
	 */
	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	/**
	 * 
	 * @param options
	 *            -> is the option parameter for the jar executor
	 */
	public void setOptions(String options) {
		this.options = options;
	}

	/**
	 * 
	 * @param round
	 *            -> is the number of the round
	 */
	public void setRound(int round) {
		this.round = round;
	}

	/**
	 * 
	 * @param logJarOutput
	 *            -> true then log the jar output
	 */
	public void setLogJarOutput(boolean logJarOutput) {
		this.logJarOutput = logJarOutput;
	}

	/**
	 * execute a jar from the test case
	 * http://stackoverflow.com/questions/15700879/how-to-run-a-java-executable-
	 * jar-in-another-java-program
	 * 
	 * @param sourceFolder
	 *            -> is a option for the jar
	 * @param targetFolder
	 *            -> is a option for the jar
	 * @return -> true if occur a error
	 */
	public boolean runTestCaseRound(String sourceFolder, String targetFolder) {
		this.sourceFolder = sourceFolder;
		this.targetFolder = targetFolder;
		return run();
	}

	/**
	 * generate the commands list for the executed jar
	 * 
	 * @param sourceFolder
	 *            -> is a option for the jar
	 * @param targetFolder
	 *            -> is a option for the jar
	 * @return list of commands
	 */
	private String[] getCommandsForTestCaseJar(String sourceFolder, String targetFolder) {

		int numberOfCommands = 6;
	
		String[] commands = new String[numberOfCommands];
		commands[0] = "java";
		commands[1] = "-jar";
		commands[2] = jarPath;
		commands[3] = sourceFolder;
		commands[4] = targetFolder;
		commands[5] = this.options;

		return commands;
	}

	@Override
	public String[] getCommand() {
		return getCommandsForTestCaseJar(this.sourceFolder, this.targetFolder);
	}

	@Override
	public String getErrorMessage() {
		String executableCommand = Arrays.toString(getCommand()).replaceAll(",", "");
		executableCommand = executableCommand.substring(1, executableCommand.length()-1);
		return StringTemplates.outputExecutorEndTitle(scenarioName, testCaseName, round,executableCommand);
	}

	@Override
	public String getInfoMessage() {
		String executableCommand = Arrays.toString(getCommand()).replaceAll(",", "");
		executableCommand = executableCommand.substring(1, executableCommand.length()-1);
		return StringTemplates.outputExecutorEndTitle(scenarioName, testCaseName, round,executableCommand);
	}

	@Override
	public boolean isLogJarOutput() {
		return this.logJarOutput;
	}
}
