package com.mgm.cbmf.controllers;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mgm.cbmf.elements.properties.PropertiesElement;
import com.mgm.cbmf.elements.properties.ZabbixPropertiesElement;
import com.mgm.cbmf.elements.resultjson.HostInformationElement;
import com.mgm.cbmf.elements.resultjson.HostsGroupInformationElement;
import com.mgm.cbmf.models.StringTemplates;
import com.mgm.cbmf.models.ZabbixAPI;

/**
 * this class managed the Zabbix hosts
 * 
 * @author bugge
 * @since 05.02.2016
 */
public class ZabbixHostsManager {

	private final static Logger LOGGER = Logger.getLogger(ZabbixHostsManager.class.getName());

	private ZabbixAPI zabbixApi;
	private ZabbixPropertiesElement properties;
	private boolean connect = true;

	/**
	 * 
	 * @param properties -> properties from this benchmark 
	 */
	public ZabbixHostsManager(PropertiesElement properties) {
		this.properties = properties.getZabbixProperties();
		this.zabbixApi = new ZabbixAPI(this.properties);
		if (!zabbixApi.isConnectedWithLogOutput()) {
			zabbixApi.close();
			this.connect = false;
		}
	}

	/**
	 * enable all zabbix hosts to monitoring
	 * 
	 * @param properties
	 */
	public static void enableHosts(PropertiesElement properties) {
		ZabbixHostsManager zabbixManager = new ZabbixHostsManager(properties);
		zabbixManager.enableHosts();
		zabbixManager.closeZabbix();
	}

	/**
	 * disable all zabbix hosts to monitoring
	 * 
	 * @param properties
	 */
	public static void disableHosts(PropertiesElement properties) {
		ZabbixHostsManager zabbixManager = new ZabbixHostsManager(properties);
		zabbixManager.disableHosts();
		zabbixManager.closeZabbix();
	}
	
	/**
	 * enable hosts
	 */
	public void enableHosts() {
		if (properties.isEnable()) {
			if (!connect) {
				LOGGER.error(StringTemplates.outputCantEnableHosts());
				return;
			}
			Map<Integer, HostInformationElement> hosts = getHosts();
			for(Integer hostID : hosts.keySet()){
				this.zabbixApi.enableHost(hostID);
			}
		}
	}

	/**
	 * disable hosts
	 */
	public void disableHosts() {
		if (properties.isEnable()) {
			if (!connect) {
				LOGGER.error(StringTemplates.outputCantDisableHosts());
				return;
			}
			Map<Integer, HostInformationElement> hosts = getHosts();
			for(Integer hostID : hosts.keySet()){
				this.zabbixApi.disableHost(hostID);
			}
		}
	}
	
	/**
	 * get hosts informations
	 * @return host informations
	 */
	private Map<Integer, HostInformationElement> getHosts() {
		Map<Integer, HostInformationElement> hosts = new HashMap<Integer, HostInformationElement>();
		HostsGroupInformationElement hostGroup = this.zabbixApi.getHostGroupInformationElement();
		if (hostGroup.getId() < 0) {
			LOGGER.error(StringTemplates.outputCantFindHostGroup());
			return hosts;
		}
		return this.zabbixApi.getHostsByGroupID(hostGroup);
	}

	/**
	 * logout and close zabbix connections
	 */
	public void closeZabbix() {
		zabbixApi.close();
		this.connect = false;
	}

}
