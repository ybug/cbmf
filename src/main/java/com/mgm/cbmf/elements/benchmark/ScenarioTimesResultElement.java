package com.mgm.cbmf.elements.benchmark;

import java.util.HashMap;
import java.util.Map;

/**
 * Contain all time results from one scenario
 * @author bugge
 * @since 28.01.2016
 */
public class ScenarioTimesResultElement {
	
	private String name = null;
	private Map<String,TestCaseTimesResultElement> testcasesTimes = new HashMap<String,TestCaseTimesResultElement>();

	public Map<String, TestCaseTimesResultElement> getTestcasesTimes() {
		return testcasesTimes;
	}

	public void setTestcasesTimes(Map<String, TestCaseTimesResultElement> testcasesTimes) {
		this.testcasesTimes = testcasesTimes;
	}
	
	public void addTestcaseTimes(String key, TestCaseTimesResultElement testcaseTimes) {
		this.testcasesTimes.put(key, testcaseTimes);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ScenarioTimesResultElement [name=" + name + ", testcasesTimes=" + testcasesTimes + "]";
	}
}
