package com.mgm.cbmf.controllers;

import com.mgm.cbmf.elements.properties.ScenarioElement;
import com.mgm.cbmf.models.executors.CleanupCommandLineExecutor;

public class CleanupManager {

	private ScenarioElement properties;
	private String refPath = null;
	private String cleanupPath = null;

	public CleanupManager(ScenarioElement properties) {
		this.properties = properties;
	}

	public void setCleanupPath(String cleanupPath) {
		this.refPath = this.cleanupPath;
		this.cleanupPath = cleanupPath;
	}

	public boolean cleanup(String generatedSourceFolderPath, boolean firstRoundInScenario, boolean lastRoundInScenario,
			boolean lastRoundInTestCase) {
		boolean error = false;
		
		
		if(!this.properties.getCompareSettings().getEnable()){
			//if comparator disable, delete last result
			error = cleanupWithRefPath(generatedSourceFolderPath, lastRoundInTestCase);
		}
		else if (this.properties.getCompareSettings().existReferenceDatasetPath()) {
			error = cleanupWithRefPath(generatedSourceFolderPath, lastRoundInTestCase);
		} else {
			error = cleanupWithoutRefPath(generatedSourceFolderPath, firstRoundInScenario, lastRoundInScenario,
					lastRoundInTestCase);
		}

		return error;
	}

	public boolean cleanupWithRefPath(String generatedSourceFolderPath, boolean lastRoundInTestCase) {
		boolean error = cleanupWithCommandLine(cleanupPath);
		if (lastRoundInTestCase) {
			error |= cleanupWithCommandLine(generatedSourceFolderPath);
		}
		return error;
	}

	public boolean cleanupWithoutRefPath(String generatedSourceFolderPath, boolean firstRoundInScenario,
			boolean lastRoundInScenario, boolean lastRoundInTestCase) {
		boolean error = false;
		if (firstRoundInScenario) {
			return false;
		}
		
		error |= cleanupWithCommandLine(refPath);
		
		if (lastRoundInScenario) {
			error |= cleanupWithCommandLine(cleanupPath);
		}

		if (lastRoundInTestCase) {
			error |= cleanupWithCommandLine(generatedSourceFolderPath);
		}
		return error;
	}

	public boolean cleanupWithCommandLine(String folderPath) {
		CleanupCommandLineExecutor execute = new CleanupCommandLineExecutor(
				properties.getCleanupSettings().getCommandLine());
		execute.setCleanUpFolderPath(folderPath);
		execute.setLogJarOutput(properties.getCleanupSettings().getLogJarOutput());
		return execute.run();
	}

}
