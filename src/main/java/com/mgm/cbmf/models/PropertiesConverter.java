package com.mgm.cbmf.models;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mgm.cbmf.elements.properties.PropertiesElement;
import com.mgm.cbmf.elements.properties.ScenarioElement;

/**
 * Convert the properties to PropertiesElement format
 * 
 * @author bugge
 * @since 21.01.2016
 */
public class PropertiesConverter {

	private final static Logger LOGGER = Logger.getLogger(PropertiesConverter.class.getName());

	private Map<String, String> propertiesMap = new HashMap<String, String>();
	private String propertiesPath;
	private PropertiesElement propertiesElement;

	/**
	 * 
	 * @param propertiesPath
	 *            -> is the file path to the properties
	 */
	public PropertiesConverter(String propertiesPath) {
		this.propertiesPath = propertiesPath;
		this.propertiesElement = new PropertiesElement(this.propertiesPath);
	}

	/**
	 * 
	 * @param propertiesMap
	 *            -> is the key value map in with key is name of the property
	 *            and value is the content from the property
	 */
	public void setPropertiesMap(Map<String, String> propertiesMap) {
		this.propertiesMap = propertiesMap;
	}

	/**
	 * convert from map to PropertiesElement
	 */
	public void convert() {
		Map<String, ScenarioElement> scenarios = new HashMap<String, ScenarioElement>();
		this.propertiesElement = new PropertiesElement(this.propertiesPath);
		for(String key : this.propertiesMap.keySet()){
			String property = this.propertiesMap.get(key); 
			if (getKeyElementNumber(key) < 1) {
				LOGGER.warn(StringTemplates.outputPropertiesConverterUnknownKey(key, property));
			} else if (isPropertyScenario(key)) {
				ScenarioElement scenario = getTestCaseElementByKeyFromTestCases(key, scenarios);
				addPropertyToScenarioElement(key, property, scenario);
				saveScenarioInScenarios(key, scenario, scenarios);
			} else if(isCopyConfigPaths(key)){
				setPropertyInCopyConfigPaths(key,property);
			} else if (isPropertyForPropertiesElement(key)) {
				setPropertyInPropertiesElement(key, property);
			} else {
				LOGGER.warn(StringTemplates.outputPropertiesConverterUnknownKey(key, property));
			}
		}
		
		for(String key : scenarios.keySet()){
			ScenarioElement scenario = scenarios.get(key);
			propertiesElement.addScenario(key, scenario);
		}
	}

	/**
	 * 
	 * @return the converted PropertiesElememt
	 */
	public PropertiesElement getPropertiesElement() {
		return this.propertiesElement;
	}

	/**
	 * 
	 * @param key
	 *            -> key from the current property
	 * @return true if the key begin with testcase
	 */
	private boolean isPropertyScenario(String key) {
		return key.split("\\.")[0].equals(StringTemplates.propertiesScenarioIndikatorKey);
	}
	
	private boolean isCopyConfigPaths(String key) {
		return key.split("\\.")[0].equals(StringTemplates.propertiesConfigFolderPathsIndikatorKey);
	}

	/**
	 * 
	 * @param key
	 *            -> key from the current property name
	 * @return true if the property begining with common
	 */
	private boolean isPropertyForPropertiesElement(String key) {
		return key.split("\\.")[0].equals(StringTemplates.propertiesPropertyIndikatorKey);
	}

	/**
	 * set the properties from type common in the propertiesElement
	 * 
	 * @param key
	 *            -> name from the property
	 * @param property
	 *            -> value from the property
	 */
	private void setPropertyInPropertiesElement(String key, String property) {
		if (getKeyElementNumber(key) < 2) {
			LOGGER.warn(StringTemplates.outputPropertiesConverterUnknownKey(key, property));
			return;
		}
		String propertyIndikator = key.split("\\.")[1];
		if (propertyIndikator.equals(StringTemplates.propertiesEvaluationResultPathIndikatorKey)) {
			this.propertiesElement.setEvaluationResultPath(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesEvaluationZipResultPathIndikatorKey)) {
			this.propertiesElement.setEvaluationZipResultPath(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesZabbixUserNameIndikatorKey)) {
			this.propertiesElement.getZabbixProperties().setUserName(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesZabbixPasswordIndikatorKey)) {
			this.propertiesElement.getZabbixProperties().setPassword(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesZabbixUrlIndikatorKey)) {
			this.propertiesElement.getZabbixProperties().setUrl(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesJarLoggerKey)) {
			this.propertiesElement.setLogJavaOutput(Boolean.parseBoolean(property));
		} else if (propertyIndikator.equals(StringTemplates.propertiesZabbixHostGroupIndikatorKey)) {
			this.propertiesElement.getZabbixProperties().setHostGroup(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesZabbixEnabelHostsIndikatorKey)) {
			this.propertiesElement.getZabbixProperties().setEnableHostAtTheStart(Boolean.parseBoolean(property));
		} else if (propertyIndikator.equals(StringTemplates.propertiesZabbixDisableHostsIndikatorKey)) {
			this.propertiesElement.getZabbixProperties().setDisableHostAtTheFinished(Boolean.parseBoolean(property));
		} else if (propertyIndikator.equals(StringTemplates.propertiesZabbixEnableIndikatorKey)) {
			this.propertiesElement.getZabbixProperties().setEnable(Boolean.parseBoolean(property));
		} else if (propertyIndikator.equals(StringTemplates.propertiesBenchmarlNameIndikatorKey)) {
			this.propertiesElement.setBenchmarkName(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesDbEnableIndikatorKey)) {
			this.propertiesElement.getDb().setEnable(Boolean.parseBoolean(property));
		} else if (propertyIndikator.equals(StringTemplates.propertiesDbUrlIndikatorKey)) {
			this.propertiesElement.getDb().setDbUrl(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesDbUserIndikatorKey)) {
			this.propertiesElement.getDb().setUser(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesDbPasswordIndikatorKey)) {
			this.propertiesElement.getDb().setPassword(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesDbJDBCDriverIndikatorKey)) {
			this.propertiesElement.getDb().setJdbcDriver(property);		
		} else {
			LOGGER.warn(StringTemplates.outputPropertiesConverterUnknownKey(key, property));
		}
	}
	
	private void setPropertyInCopyConfigPaths(String key, String property) {
		if (getKeyElementNumber(key) < 2) {
			LOGGER.warn(StringTemplates.outputPropertiesConverterUnknownKey(key, property));
			return;
		}
		String propertyIndikator = key.split("\\.")[1];
		this.propertiesElement.addCopyConfigFilePath(propertyIndikator, property);
	}

	/**
	 * get the CaseElement from TestCases by the key
	 * 
	 * @param key
	 *            -> is the properties key
	 * @param testCases
	 *            -> saved all testCases
	 * @return the current TestCaseElement or a empty TestCaseElement
	 */
	private ScenarioElement getTestCaseElementByKeyFromTestCases(String key, Map<String, ScenarioElement> testCases) {
		ScenarioElement returnElement = testCases.get(key.split("\\.")[1]);
		if(returnElement == null){
			return new ScenarioElement();
		}
		return returnElement;
	}

	/**
	 * add to the scenario the property
	 * 
	 * @param scenario
	 *            -> this element get the new property
	 * @param key
	 *            -> is the property key
	 * @param property
	 *            -> is the value from the key
	 */
	private void addPropertyToScenarioElement(String key, String property, ScenarioElement scenario) {
		if (getKeyElementNumber(key) < 3) {
			LOGGER.warn(StringTemplates.outputPropertiesConverterUnknownKey(key, property));
			return;
		}
		String propertyIndikator = key.split("\\.")[2];
		if (propertyIndikator.equals(StringTemplates.propertiesScenarioNameIndikatorKey)) {
			scenario.setName(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesSourceFolderPathIndikatorKey)) {
			scenario.setSourceFolderPath(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesRoundIndikatorKey)) {
			scenario.setRoundNumber(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesCompareJarPathKey)) {
			scenario.getCompareSettings().setJarPath(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesCompareJarOptionsKey)) {
			scenario.getCompareSettings().setJarOptions(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesCompareEnableKey)) {
			scenario.getCompareSettings().setEnable(Boolean.parseBoolean(property));
		} else if (propertyIndikator.equals(StringTemplates.propertiesCompareLogOutputKey)) {
			scenario.getCompareSettings().setLogJarOutput(Boolean.parseBoolean(property));
		} else if (propertyIndikator.equals(StringTemplates.propertiesCompareReportPathKey)) {
			scenario.getCompareSettings().setReportPath(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesCompareCommandLineKey)) {
			scenario.getCompareSettings().setCommandLine(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesCompareReferenceDatasetPathKey)) {
			scenario.getCompareSettings().setReferenceDatasetPath(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesGeneratorCommandLineKey)) {
			scenario.getGeneratorSettings().setCommandLine(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesGeneratorLogOutputKey)) {
			scenario.getGeneratorSettings().setLogJarOutput(Boolean.parseBoolean(property));
		} else if (propertyIndikator.equals(StringTemplates.propertiesGeneratorEnableKey)) {
			scenario.getGeneratorSettings().setEnable(Boolean.parseBoolean(property));
		} else if (propertyIndikator.equals(StringTemplates.propertiesCleanupCommandLineKey)) {
			scenario.getCleanupSettings().setCommandLine(property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesCleanupLogOutputKey)) {
			scenario.getCleanupSettings().setLogJarOutput(Boolean.parseBoolean(property));
		} else if (propertyIndikator.equals(StringTemplates.propertiesCleanupEnableKey)) {
			scenario.getCleanupSettings().setEnable(Boolean.parseBoolean(property));
		} else if (propertyIndikator.equals(StringTemplates.propertiesTestCaseIndikatorKey)) {
			addPropertyToTestCaseElement(key, property, scenario);
		} else if (propertyIndikator.equals(StringTemplates.propertiesDefaultValueKey)) {
			addPropertyToDefaultValueElement(key, property, scenario);
		} else {
			LOGGER.warn(StringTemplates.outputPropertiesConverterUnknownKey(key, property));
		}
	}

	/**
	 * add to the testCase the property
	 * 
	 * @param scenario
	 *            -> this element get the new property
	 * @param key
	 *            -> is the property key
	 * @param property
	 *            -> is the property to the key
	 */
	private void addPropertyToTestCaseElement(String key, String property, ScenarioElement scenario) {
		if (getKeyElementNumber(key) < 5) {
			LOGGER.warn(StringTemplates.outputPropertiesConverterUnknownKey(key, property));
			return;
		}
		String caseIndikator = key.split("\\.")[3];
		String propertyIndikator = key.split("\\.")[4];
		if (propertyIndikator.equals(StringTemplates.propertiesTestCaseNameIndikatorKey)) {
			scenario.setInTestCaseName(caseIndikator, property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesTestCaseJarPathIndikatorKey)) {
			scenario.setInTestCaseJarPath(caseIndikator, property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesTestCaseJarOptionIndikatorKey)) {
			scenario.setInTestCaseJarOptions(caseIndikator, property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesTestCaseCommandLineIndikatorKey)) {
			scenario.setInTestCaseCommandLine(caseIndikator, property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesTestCaseResultFolderPathIndikatorKey)) {
			scenario.setInTestCaseResultFolderPath(caseIndikator, property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesTestCaseBeforeIndikatorKey)) {
			scenario.setInTestCaseBeforeCommand(caseIndikator, property); 
		} else if (propertyIndikator.equals(StringTemplates.propertiesTestCaseAfterIndikatorKey)) {
			scenario.setInTestCaseAfterCommand(caseIndikator, property); 
		} else {
			LOGGER.warn(StringTemplates.outputPropertiesConverterUnknownKey(key, property));
		}
	}
	
	/**
	 * add to the default value the property
	 * @param scenario
	 *            -> this element get the new property
	 * @param key
	 *            -> is the property key
	 * @param property
	 *            -> is the property to the key
	 */
	private void addPropertyToDefaultValueElement(String key, String property, ScenarioElement scenario) {
		if (getKeyElementNumber(key) < 5) {
			LOGGER.warn(StringTemplates.outputPropertiesConverterUnknownKey(key, property));
			return;
		}
		String defaultValueIndikator = key.split("\\.")[3];
		String propertyIndikator = key.split("\\.")[4];
		if (propertyIndikator.equals(StringTemplates.propertiesDefaultValueNameKey)) {
			scenario.setInDefaultValueName(defaultValueIndikator, property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesDefaultValueValueKey)) {
			scenario.setInDefaultValueValue(defaultValueIndikator, property);
		} else if (propertyIndikator.equals(StringTemplates.propertiesDefaultValueUnitKey)) {
			scenario.setInDefaultValueUnit(defaultValueIndikator, property);
		} else {
			LOGGER.warn(StringTemplates.outputPropertiesConverterUnknownKey(key, property));
		}
	}

	/**
	 * save in a map as key the scenario name and as value the given scenario
	 * 
	 * @param key
	 *            -> is the property key -> will extract the scenario name
	 * @param scenario
	 *            -> to save in scenarios
	 * @param scenarios
	 *            -> is the map
	 */
	private void saveScenarioInScenarios(String key, ScenarioElement scenario, Map<String, ScenarioElement> scenarios) {
		scenarios.put(key.split("\\.")[1], scenario);
	}

	/**
	 * get the segments size from the property key
	 * 
	 * @param key
	 *            -> is the property name
	 * @return number of segments
	 */
	private int getKeyElementNumber(String key) {
		return key.split("\\.").length;
	}
}
