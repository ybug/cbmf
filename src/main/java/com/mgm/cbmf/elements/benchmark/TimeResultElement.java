package com.mgm.cbmf.elements.benchmark;

import java.sql.Timestamp;

/**
 * this class contain the time for one run
 * @author bugge
 * @since 28.01.2016
 */
public class TimeResultElement {
	
	private Timestamp startTime;
	private Timestamp endTime;
	private Boolean executeError;
	private Boolean comparatorError;
	private Boolean comperatorEnable;
	private Boolean cleanupError;
	private Boolean cleanupEnable;
	
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	public Boolean isExecuteError() {
		return executeError;
	}
	public void setExecuteError(Boolean error) {
		this.executeError = error;
	}
	public Boolean getComparatorError() {
		return comparatorError;
	}
	public void setComparatorError(Boolean comparatorError) {
		this.comparatorError = comparatorError;
	}
	public Boolean getComperatorEnable() {
		return comperatorEnable;
	}
	public void setComperatorEnable(Boolean comperatorEnable) {
		this.comperatorEnable = comperatorEnable;
	}
	public Boolean getCleanupError() {
		return cleanupError;
	}
	public void setCleanupError(Boolean cleanupError) {
		this.cleanupError = cleanupError;
	}
	public Boolean getCleanupEnable() {
		return cleanupEnable;
	}
	public void setCleanupEnable(Boolean cleanupEnable) {
		this.cleanupEnable = cleanupEnable;
	}
	public Boolean getExecuteError() {
		return executeError;
	}
	@Override
	public String toString() {
		return "TimeResultElement [startTime=" + startTime + ", endTime=" + endTime + ", executeError=" + executeError
				+ ", comparatorError=" + comparatorError + ", comperatorEnable=" + comperatorEnable + ", cleanupError="
				+ cleanupError + ", cleanupEnable=" + cleanupEnable + "]";
	}
}
