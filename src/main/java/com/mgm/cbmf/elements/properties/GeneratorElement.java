package com.mgm.cbmf.elements.properties;

/**
 * is the properties class to set the generator settings
 * @author bugge
 * @since 14.06.2016
 */
public class GeneratorElement {
	
	private Boolean enable = false;
	private Boolean logJarOutput = true;
	private String commandLine = null;
	
	public Boolean getEnable() {
		return enable;
	}
	public void setEnable(Boolean enable) {
		this.enable = enable;
	}
	public Boolean getLogJarOutput() {
		return logJarOutput;
	}
	public void setLogJarOutput(Boolean logJarOutput) {
		this.logJarOutput = logJarOutput;
	}
	public String getCommandLine() {
		return commandLine;
	}
	public void setCommandLine(String commandLine) {
		this.commandLine = commandLine;
	}
	
	public boolean isValid(){
		if(enable == true && commandLine == null){
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "GeneratorElement [enable=" + enable + ", logJarOutput=" + logJarOutput + ", commandLine=" + commandLine
				+ "]";
	}
}
