package com.mgm.cbmf.models.executors;

import com.mgm.cbmf.models.StringTemplates;

public class TestCaseAfterCommandLineExecutor extends CommandLineExecutor{

	private final String command;
	private final String testCaseName;
	private boolean logJarOutput = true;
	
	private final boolean ERROR = true;
	private final boolean SUCCESFUL= !ERROR;
	
	public TestCaseAfterCommandLineExecutor(String testCaseName, String command){
		this.command = command;
		this.testCaseName = testCaseName;
	}

	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public String getErrorMessage() {
		return StringTemplates.outputExecutorAfterTitle(command,testCaseName, ERROR);
	}

	@Override
	public String getInfoMessage() {
		return StringTemplates.outputExecutorAfterTitle(command,testCaseName, SUCCESFUL);
	}

	@Override
	public boolean isLogExecuteOutput() {
		return logJarOutput;
	}

	public void setLogJarOutput(boolean logJarOutput) {
		this.logJarOutput = logJarOutput;
	}
}
