package com.mgm.cbmf.models;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Map.Entry;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mgm.cbmf.elements.properties.PropertiesElement;
import com.mgm.cbmf.elements.properties.ScenarioElement;
import com.mgm.cbmf.elements.resultjson.BenchmarkResultsElement;

/**
 * write out the results 
 * - benchmark results
 * - comparator reports
 * - log file 
 * - properties file 
 * @author bugge
 * @since 12.02.2016
 */
public class ResultWriter {
	
	private final static Logger LOGGER = Logger.getLogger(ResultWriter.class.getName()); 

	private final PropertiesElement properties;
	private  ObjectMapper mapper = new ObjectMapper();
	
	/**
	 * write out benchmark results, comparator reports and properties file 
	 * @param properties -> properties from the Benchmark
	 * @param results -> results from the benchmark witch zabbix results.
	 */
	public static void write(PropertiesElement properties, BenchmarkResultsElement results){
		ResultWriter writer = new ResultWriter(properties);
		writer.createResultFolder();
		writer.writeResultsInJsonFile(results);
		writer.copyPropertiesFile();
		writer.copyComperatorReportForEachScenarios();
		writer.copyAllConfigFiles();
	}
	
	/**
	 * copy the log file in the result folder 
	 * @param properties -> properties from this benchmark 
	 */
	public static void cutLogFile(PropertiesElement properties){
		ResultWriter writer = new ResultWriter(properties);
		writer.createResultFolder();
		writer.copyLoggerFile();
		writer.deleteLoggerFile();
	}
	
	/**
	 * delete the last result folder
	 * @param properties -> properties from this benchmark 
	 */
	public static void deleteLastResultFolder(PropertiesElement properties){
		if(FileFolderOperations.existFileOrFolder(properties.getEvaluationResultPath())){
			FileFolderOperations.deleteFolder(properties.getEvaluationResultPath());
		}
	}
	
	/**
	 * 
	 * @param properties -> properties from this benchmark
	 */
	private ResultWriter(PropertiesElement properties){
		this.properties = properties;
	}
	
	/**
	 * write out the results from the benchmark
	 * @param results -> results from the benchmark with zabbix results
	 */
	private void writeResultsInJsonFile(BenchmarkResultsElement results){
		try {
			mapper.writeValue(new File(getResultFilePath()), results);
		} catch (JsonGenerationException e) {
			LOGGER.error(StringTemplates.outputCantWriteOutResultsInJsonFile(),e);
		} catch (JsonMappingException e) {
			LOGGER.error(StringTemplates.outputCantWriteOutResultsInJsonFile(),e);
		} catch (IOException e) {
			LOGGER.error(StringTemplates.outputCantWriteOutResultsInJsonFile(),e);
		}
	}
	
	/**
	 * 
	 * @return the file path from the results file
	 */
	private String getResultFilePath(){
		return Paths.get(getResultFolderPath(), DefaultValues.resultJsonFileName).toString();
	}
	
	/**
	 * create the result folder
	 */
	private void createResultFolder(){
		FileFolderOperations.creatFolder(getResultFolderPath());
	}
	
	/**
	 * 
	 * @return -> get the result folder path. this folder contains all results from the benchmark, like log file and comparator reports
	 */
	private String getResultFolderPath(){
		return Paths.get(properties.getEvaluationResultPath(), new SimpleDateFormat(DefaultValues.resultFolderDateFormat).format(properties.getStartDate())+getConfigFileAnomalyString()).toString();
	}
	
	private String getConfigFileAnomalyString(){
		String defaultFileName = DefaultValues.propertiesFileName;
		int defaultFileNamePointIndex = defaultFileName.indexOf(".");
		String defaultFileTop = defaultFileName.substring(0, defaultFileNamePointIndex);
		String defaultFileTail = defaultFileName.substring(defaultFileNamePointIndex, defaultFileName.length());
		return new File(properties.getPropertiesPath()).getName().replace(defaultFileTop, "").replace(defaultFileTail, "");
	}
	
	private void copyAllConfigFiles(){
		FileFolderOperations.creatFolder(Paths.get(getResultFolderPath(),DefaultValues.resultConfigFolderName).toString());
		for(Entry<String, String> entry: properties.getCopyConfigFilePaths().entrySet()){
			String targetPath = Paths.get(getResultFolderPath(),DefaultValues.resultConfigFolderName,entry.getKey(),FileFolderOperations.getFileNameFromPath(entry.getValue())).toString();
			FileFolderOperations.creatFolder(targetPath);
			FileFolderOperations.copyFolder(entry.getValue(), targetPath);
		}
	}
	
	/**
	 * copy the properties file to the result folder
	 */
	private void copyPropertiesFile(){
		FileFolderOperations.copyFile(properties.getPropertiesPath(),
				Paths.get(getResultFolderPath(),
						FileFolderOperations.getFileNameFromPath(properties.getPropertiesPath())).toString());
	}
	
	/**
	 * copy the reports from the comparators for each scenario to the result folder
	 */
	private void copyComperatorReportForEachScenarios(){
		for(String scenarioName : properties.getScenarios().keySet()){
			ScenarioElement scenario = properties.getScenarios().get(scenarioName);
			if(scenario.getCompareSettings().existReportPath()&& scenario.getCompareSettings().getEnable()){
				copyAndDeleteComperatorReportForScenario(scenario.getCompareSettings().getReportPath(),scenario.getName());
			}
		}
	}
	
	/**
	 * copy the report file from the comparator from one scenario (delete)
	 * @param reportPath -> is the path to find the report
	 * @param scenarioName -> is the name from the scenario
	 */
	private void copyAndDeleteComperatorReportForScenario(String reportPath,String scenarioName){
		if(FileFolderOperations.existFile(reportPath)){
			FileFolderOperations.creatFolder(Paths.get(getResultFolderPath(),scenarioName).toString());
			FileFolderOperations.copyFile(reportPath,Paths.get(getResultFolderPath(),scenarioName,FileFolderOperations.getFileNameFromPath(reportPath)).toString());
			try {
				FileFolderOperations.deleteFile(reportPath);
			} catch (IOException e) {
				LOGGER.error(StringTemplates.outputCantDeleteFolder(reportPath));
			}
		}else{
			LOGGER.error(StringTemplates.outputFileDoesntExist(reportPath));
		}
	}
	
	/**
	 * copy the log file to the result folder
	 */
	private void copyLoggerFile(){
		Logger logger = Logger.getRootLogger();
		FileAppender appender = (FileAppender)logger.getAppender("FILE");
		FileFolderOperations.copyFile(appender.getFile(),Paths.get(getResultFolderPath(),DefaultValues.logFileName).toString());
	}
	
	/**
	 * delete the log file (not the log file in the result folder)
	 */
	private void deleteLoggerFile(){
		Logger logger = Logger.getRootLogger();
		FileAppender appender = (FileAppender)logger.getAppender("FILE");
		try {
			FileFolderOperations.deleteFile(appender.getFile());
		} catch (IOException e) {
			LOGGER.error(StringTemplates.outputCantDeleteLogFile(),e);
		}
	}
}
