package com.mgm.cbmf.elements.properties;

import org.apache.log4j.Logger;

import com.mgm.cbmf.models.DefaultValues;
import com.mgm.cbmf.models.StringTemplates;

/**
 * contains all properties for the zabbix api
 * @author bugge
 * @since 04.02.2016
 */
public class ZabbixPropertiesElement {
	
	private final static Logger LOGGER = Logger.getLogger(ZabbixPropertiesElement.class.getName());
	
	private String userName = null;
	private String password = null;
	private String url = null;
	private String hostGroup = null;
	private boolean enableHostAtTheStart = DefaultValues.zabbixEnableHostAtTheStart;
	private boolean disableHostAtTheFinished = DefaultValues.zabbixDisableHostAtTheFinished;
	private boolean enable=DefaultValues.zabbixEnable;
	
	public ZabbixPropertiesElement() {
		super();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getHostGroup() {
		return hostGroup;
	}

	public void setHostGroup(String hostGroup) {
		this.hostGroup = hostGroup;
	}
	
	public boolean isEnableHostAtTheStart() {
		if(isEnable() == true){
			return enableHostAtTheStart;
		}
		return false;
	}

	public void setEnableHostAtTheStart(boolean enableHostAtTheStart) {
		this.enableHostAtTheStart = enableHostAtTheStart;
	}

	public boolean isDisableHostAtTheFinished() {
		if(isEnable() == true){
			return disableHostAtTheFinished;
		}
		return false;
	}

	public void setDisableHostAtTheFinished(boolean disableHostAtTheFinished) {
		this.disableHostAtTheFinished = disableHostAtTheFinished;
	}
	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public boolean isValid(){
		if(isEnable() == false){
			return true;
		}
		else if(userName == null){
			LOGGER.error(StringTemplates
					.outputPropertiesElementNotValid(StringTemplates.propertiesZabbixUserNameIndikatorKey));
			return false;
		}
		else if(password == null){
			LOGGER.error(StringTemplates
					.outputPropertiesElementNotValid(StringTemplates.propertiesZabbixPasswordIndikatorKey));
			return false;
		}
		else if(url == null){
			LOGGER.error(StringTemplates
					.outputPropertiesElementNotValid(StringTemplates.propertiesZabbixUrlIndikatorKey));
			return false;
		}
		else if(hostGroup == null){
			LOGGER.error(StringTemplates
					.outputPropertiesElementNotValid(StringTemplates.propertiesZabbixHostGroupIndikatorKey));
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ZabbixPropertiesElement [userName=" + userName + ", password=" + password + ", url=" + url
				+ ", hostGroup=" + hostGroup + ", enableHostAtTheStart=" + enableHostAtTheStart
				+ ", disableHostAtTheFinished=" + disableHostAtTheFinished + ", enable=" + enable + "]";
	}
}
