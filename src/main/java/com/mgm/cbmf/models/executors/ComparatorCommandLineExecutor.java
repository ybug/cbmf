package com.mgm.cbmf.models.executors;

import com.mgm.cbmf.models.DefaultValues;
import com.mgm.cbmf.models.StringTemplates;

public class ComparatorCommandLineExecutor extends CommandLineExecutor {

	private final String command;
	private String referenceResultsPath = "";
	private String targetResultsPath = "";
	private boolean firstRoundInScenario = false;
	private String reportPath = "";
	
	private boolean logJarOutput = true;
	
	private final boolean ERROR = true;
	private final boolean SUCCESFUL= !ERROR;
	
	public ComparatorCommandLineExecutor(String command) {
		this.command = command;
	}
	
	@Override
	public String getCommand() {
		return command.replaceAll(DefaultValues.firstResultPlaceholders, Boolean.toString(firstRoundInScenario))
				.replaceAll(DefaultValues.referencePathPlaceholders, referenceResultsPath)
				.replaceAll(DefaultValues.resultPathPlaceholders, targetResultsPath)
				.replaceAll(DefaultValues.reportPathPlaceholders, reportPath);
	}

	@Override
	public String getErrorMessage() {
		return StringTemplates.outputExecutorComperatorEndTitle(referenceResultsPath, targetResultsPath,ERROR);
	}

	@Override
	public String getInfoMessage() {
		return StringTemplates.outputExecutorComperatorEndTitle(referenceResultsPath, targetResultsPath,SUCCESFUL);
	}

	@Override
	public boolean isLogExecuteOutput() {
		return this.logJarOutput;
	}



	public void setReferenceResultsPath(String referenceResultsPath) {
		this.referenceResultsPath = referenceResultsPath;
	}



	public void setTargetResultsPath(String targetResultsPath) {
		this.targetResultsPath = targetResultsPath;
	}



	public void setFirstRoundInScenario(boolean firstRoundInScenario) {
		this.firstRoundInScenario = firstRoundInScenario;
	}



	public void setReportPath(String reportPath) {
		this.reportPath = reportPath;
	}



	public void setLogJarOutput(boolean logJarOutput) {
		this.logJarOutput = logJarOutput;
	}
}
