package com.mgm.cbmf.models.executors;
import com.mgm.cbmf.models.StringTemplates;

public class ComparatorJarExecutor extends JarExecutor{

	private String jarPath;
	private String options = "";
	
	private String referenceResultsPath = "";
	private String targetResultsPath = "";
	private boolean firstRoundInScenario = false;
	private String reportPath = "";
	
	private final boolean ERROR = true;
	private final boolean SUCCESFUL= !ERROR;

	private boolean logJarOutput = false;

	/**
	 * 
	 * @param jarPath
	 *            -> is the path to the executable jar
	 */
	public ComparatorJarExecutor(String jarPath) {
		this.jarPath = jarPath;
	}

	/**
	 * 
	 * @param options
	 *            -> is the option parameter for the jar executor
	 */
	public void setOptions(String options) {
		this.options = options;
	}


	/**
	 * 
	 * @param logJarOutput
	 *            -> true then log the jar output
	 */
	public void setLogJarOutput(boolean logJarOutput) {
		this.logJarOutput = logJarOutput;
	}


	/**
	 * execute the comparator jar for one scenario
	 * 
	 * @param referenceResultsPath
	 *            -> send to the comparator
	 * @param targetResultsPath
	 *            -> send to the comparator
	 * @param firstRoundInScenario
	 *            -> send to the comparator
	 * @param reportPath
	 *            -> send to the comparator
	 * @return true if occur a error or don't match the results.
	 */
	public boolean runComparator(String referenceResultsPath, String targetResultsPath, boolean firstRoundInScenario,
			String reportPath) {
		this.referenceResultsPath = referenceResultsPath;
		this.targetResultsPath = targetResultsPath;
		this.firstRoundInScenario = firstRoundInScenario;
		this.reportPath = reportPath;
		
		return run();
	}

	/**
	 * generate the commands list for the comparator jar
	 * @param referenceResultsPath -> is the option form the jar
	 * @param targetResultsPath  -> is the option form the jar
	 * @param firstRoundInScenario  -> is the option form the jar
	 * @param reportPath  -> is the option form the jar
	 * @return list of commands
	 */
	private String[] getCommandsForComparatorJar(String referenceResultsPath, String targetResultsPath,
			boolean firstRoundInScenario, String reportPath) {

		int numberOfCommands = 8;
		String[] commands = new String[numberOfCommands];
		commands[0] = "java";
		commands[1] = "-jar";
		commands[2] = jarPath;
		commands[3] = referenceResultsPath;
		commands[4] = targetResultsPath;
		commands[5] = Boolean.toString(firstRoundInScenario);
		commands[6] = reportPath;
		commands[7] = this.options;
		return commands;
	}

	@Override
	public String[] getCommand() {
		return getCommandsForComparatorJar(this.referenceResultsPath,this.targetResultsPath,this.firstRoundInScenario,this.reportPath);
	}

	@Override
	public String getErrorMessage() {
		return StringTemplates.outputExecutorComperatorEndTitle(referenceResultsPath, targetResultsPath,ERROR);
	}

	@Override
	public String getInfoMessage() {
		return StringTemplates.outputExecutorComperatorEndTitle(referenceResultsPath, targetResultsPath,SUCCESFUL);
	}

	@Override
	public boolean isLogJarOutput() {
		return logJarOutput;
	}
}
