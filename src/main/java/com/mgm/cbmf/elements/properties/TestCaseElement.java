package com.mgm.cbmf.elements.properties;

import org.apache.log4j.Logger;

import com.mgm.cbmf.models.StringTemplates;

/**
 * contain one test case
 * 
 * @author bugge
 * @since 27.01.2016
 */
public class TestCaseElement {
	private final static Logger LOGGER = Logger.getLogger(TestCaseElement.class.getName());

	private String name = null;
	private String resultFolderPath = null;
	private String jarPath = null;
	private String jarOptions = null;
	private String commandLine = null;
	private String caseIndicator = null;
	private String beforeCommand = null;
	private String afterCommand = null;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResultFolderPath() {
		return resultFolderPath;
	}

	public void setResultFolderPath(String resultFolderPath) {
		this.resultFolderPath = resultFolderPath;
	}

	public String getJarPath() {
		return jarPath;
	}

	public void setJarPath(String jarPath) {
		this.jarPath = jarPath;
	}
	
	public String getJarOptions() {
		return jarOptions;
	}

	public void setJarOptions(String jarOptions) {
		this.jarOptions = jarOptions;
	}
	
	public String getCommandLine() {
		return commandLine;
	}

	public void setCommandLine(String commandLine) {
		this.commandLine = commandLine;
	}
	
	public String getCaseIndicator() {
		return caseIndicator;
	}

	public void setCaseIndicator(String caseIndicator) {
		this.caseIndicator = caseIndicator;
	}

	public String getBeforeCommand() {
		return beforeCommand;
	}

	public void setBeforeCommand(String beforeCommand) {
		this.beforeCommand = beforeCommand;
	}

	public String getAfterCommand() {
		return afterCommand;
	}

	public void setAfterCommand(String afterCommand) {
		this.afterCommand = afterCommand;
	}
	
	public boolean existBeforeCommand(){
		return (getBeforeCommand() == null) ? false : true;
	}
	
	public boolean existAfterCommand(){
		return (getAfterCommand() == null) ? false : true;
	}

	public boolean existJarPath(){
		if(commandLine == null && jarPath != null){
			return true;
		}
		return false;
	}
	
	public boolean existCommandLine(){
		if(commandLine != null){
			return true;
		}
		return false;
	}

	public boolean existJarOptions(){
		if(this.jarOptions != null){
			return true;
		}
		return false;
	}

	public boolean isValid(String propertyKey) {
		if (name == null) {
			LOGGER.error(StringTemplates
					.outputTestCaseElementNotValid(StringTemplates.propertiesTestCaseNameIndikatorKey, propertyKey));
			return false;
		}
		if (resultFolderPath == null) {
			LOGGER.error(StringTemplates.outputTestCaseElementNotValid(
					StringTemplates.propertiesTestCaseResultFolderPathIndikatorKey, propertyKey));
			return false;
		}
		if(commandLine == null && jarPath == null){
			LOGGER.error(StringTemplates
					.outputTestCaseElementNotValid(StringTemplates.outputAND(StringTemplates.propertiesTestCaseJarPathIndikatorKey, StringTemplates.propertiesTestCaseCommandLineIndikatorKey), propertyKey));
			return false;
		}else if(commandLine != null && jarPath != null){
			LOGGER.warn(StringTemplates.outputWillIgnoreThenIsSet(StringTemplates.propertiesTestCaseJarPathIndikatorKey, StringTemplates.propertiesTestCaseCommandLineIndikatorKey, propertyKey));
		}
		return true;
	}

	@Override
	public String toString() {
		return "TestCaseElement [name=" + name + ", resultFolderPath=" + resultFolderPath + ", jarPath=" + jarPath
				+ ", jarOptions=" + jarOptions + ", commandLine=" + commandLine + ", caseIndicator=" + caseIndicator
				+ "]";
	}
}
