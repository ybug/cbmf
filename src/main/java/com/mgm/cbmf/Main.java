package com.mgm.cbmf;

import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Logger;

import com.github.jankroken.commandline.CommandLineParser;
import com.github.jankroken.commandline.OptionStyle;
import com.mgm.cbmf.controllers.BenchmarkManager;
import com.mgm.cbmf.controllers.PropertiesManager;
import com.mgm.cbmf.controllers.ResultCollectorManager;
import com.mgm.cbmf.controllers.ZabbixHostsManager;
import com.mgm.cbmf.elements.benchmark.BenchmarkTimesResultElement;
import com.mgm.cbmf.elements.properties.Arguments;
import com.mgm.cbmf.models.ResultWriter;
import com.mgm.cbmf.models.StringTemplates;
import com.mgm.cbmf.models.ZipOperations;

/**
 * This project is a benchmark for software to run in cluster. 
 * About Zabbix will monitoring each server in the cluster.  
 * @author bugge
 * @since 02.03.2016
 */
public class Main {
	
	private final static Logger LOGGER = Logger.getLogger(Main.class.getName()); 
	
	public static void main(String[] args) {
		Arguments arguments = new Arguments();
		try {
			arguments = CommandLineParser.parse(Arguments.class, args, OptionStyle.SIMPLE);
		} catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
			LOGGER.warn(e);
		}
		
		boolean error = false;
	
		LOGGER.info(StringTemplates.outputStartBenchmarkTitle);
		LOGGER.info(StringTemplates.outputMainPropertiesPartTitle);
		PropertiesManager properties = new PropertiesManager(arguments.getConfigPath());
		error = properties.loadAndConvertPropertiesFromFile();
		
		if(properties.getProperties().getZabbixProperties().isEnableHostAtTheStart()){
			ZabbixHostsManager.enableHosts(properties.getProperties());
		}
		
		if(!error){
			LOGGER.info(StringTemplates.outputMainBenchmarkPartTitle);
			BenchmarkManager benchmark = new BenchmarkManager(properties.getProperties());
			error = benchmark.run();
			BenchmarkTimesResultElement results = benchmark.getResults();
			LOGGER.info(StringTemplates.outputGetZabbixResultsPartTitle);
			ResultCollectorManager collector = new ResultCollectorManager(properties.getProperties(),results);
			collector.collect();
			collector.closeZabbix();
			LOGGER.info(StringTemplates.outputWriteOutResultsPartTitle);
			ResultWriter.deleteLastResultFolder(properties.getProperties());
			ResultWriter.write(properties.getProperties(), collector.getResults());
		}
		
		if(properties.getProperties().getZabbixProperties().isDisableHostAtTheFinished()){
			ZabbixHostsManager.disableHosts(properties.getProperties());
		}
		
		LOGGER.info(StringTemplates.outputEndBenchmarkTitle);
		ResultWriter.cutLogFile(properties.getProperties());
		ZipOperations.zipResultFolder(properties.getProperties());
	}
}
