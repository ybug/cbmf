package com.mgm.cbmf.elements.benchmark;

import java.util.ArrayList;
import java.util.List;

/**
 * this class contain all times from all runs
 * @author bugge
 * @since 28.01.2016
 */
public class TestCaseTimesResultElement {
	
	private String name = null;
	private Boolean generatorError = false;
	private Boolean generatorEnable = false;
	private List<TimeResultElement> times = new ArrayList<TimeResultElement>();

	public List<TimeResultElement> getTimes() {
		return times;
	}

	public void setTimes(List<TimeResultElement> times) {
		this.times = times;
	}
	
	public void addTime(TimeResultElement time){
		this.times.add(time);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Boolean getGeneratorError() {
		return generatorError;
	}

	public void setGeneratorError(Boolean generatorError) {
		this.generatorError = generatorError;
	}

	public Boolean getGeneratorEnable() {
		return generatorEnable;
	}

	public void setGeneratorEnable(Boolean generatorEnable) {
		this.generatorEnable = generatorEnable;
	}

	@Override
	public String toString() {
		return "TestCaseTimesResultElement [name=" + name + ", generatorError=" + generatorError + ", generatorEnable="
				+ generatorEnable + ", times=" + times + "]";
	}
}
