package com.mgm.cbmf.elements.properties;

import java.nio.file.Paths;

import com.github.jankroken.commandline.annotations.LongSwitch;
import com.github.jankroken.commandline.annotations.Option;
import com.github.jankroken.commandline.annotations.ShortSwitch;
import com.github.jankroken.commandline.annotations.SingleArgument;
import com.mgm.cbmf.models.DefaultValues;
import com.mgm.cbmf.models.StringTemplates;

public class Arguments {
	
	private static String configPath = Paths.get(DefaultValues.propertiesFolderPath,DefaultValues.propertiesFileName).toString();

	public String getConfigPath() {
		return configPath;
	}

	@Option
	@LongSwitch(StringTemplates.ARGUMENTS_CONFIG_PATH_KEY_LONG)
	@ShortSwitch(StringTemplates.ARGUMENTS_CONFIG_PATH_KEY_SHORT)
	@SingleArgument
	public void setConfigPath(String configPath) {
		Arguments.configPath = configPath;
	}

	@Override
	public String toString() {
		return "Arguments [configPath=" + configPath + "]";
	}
	
}
