package com.mgm.cbmf.controllers;

import com.mgm.cbmf.elements.properties.ScenarioElement;
import com.mgm.cbmf.models.executors.GeneratorCommandLineExecutor;

public class GeneratorManager {
	
	private ScenarioElement properties;

	public GeneratorManager(ScenarioElement properties) {
		this.properties = properties;
	}

	public boolean generate(String generateSourcePath) {
		return generateWithCommandLine(generateSourcePath);
	}

	private boolean generateWithCommandLine(String folderPath) {
		GeneratorCommandLineExecutor executor = new GeneratorCommandLineExecutor(properties.getGeneratorSettings().getCommandLine());
		executor.setGeneratedSourcePath(folderPath);
		executor.setLogJarOutput(properties.getGeneratorSettings().getLogJarOutput());
		return executor.run();
	}

}
