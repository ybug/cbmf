package com.mgm.cbmf.models.executors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import com.mgm.cbmf.models.StringTemplates;

/**
 * this class log the jar output (error)
 * 
 * @author bugge
 * @since 28.01.2016
 */
public class JarExecutorLoggerError implements Runnable {
	private final static Logger LOGGER = Logger.getLogger(JarExecutorLoggerError.class.getName());
	private BufferedReader reader;

	/**
	 * 
	 * @param is
	 *            -> input stream from the jar
	 */
	public JarExecutorLoggerError(InputStream is) {
		this.reader = new BufferedReader(new InputStreamReader(is));
	}
	
	/**
	 * write the input stream in log
	 */
	public void run() {
		try {
			String line = reader.readLine();
			while (line != null) {
				LOGGER.error(StringTemplates.outputJarExecutorLogger(line));
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
