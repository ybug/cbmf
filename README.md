# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


### Requirement Framework ###

* Maven 3
* min Java 7
* Zabbix 3.0 LTS

### Requirement Alluxio Benchmark ###

* Maven 3
* min Java 7
* Python 2.7
* Flink 1.0.1
* Alluxio 1.1.0
* HDP 2.4
* Spark 1.6.0
* Hadoop 2.7.1 (HDFS 2.7.1)
* VIM
* bzip2
* R  (r-base)
