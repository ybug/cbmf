package com.mgm.cbmf.controllers;

import java.util.TreeSet;

import com.mgm.cbmf.elements.benchmark.BenchmarkTimesResultElement;
import com.mgm.cbmf.elements.benchmark.ScenarioTimesResultElement;
import com.mgm.cbmf.elements.benchmark.TestCaseTimesResultElement;
import com.mgm.cbmf.elements.benchmark.TimeResultElement;
import com.mgm.cbmf.elements.properties.PropertiesElement;
import com.mgm.cbmf.elements.properties.ScenarioElement;
import com.mgm.cbmf.elements.properties.TestCaseElement;
import com.mgm.cbmf.models.DefaultValues;
import com.mgm.cbmf.models.executors.BenchmarkCommandLineExecutor;
import com.mgm.cbmf.models.executors.BenchmarkJarExecutor;
import com.mgm.cbmf.models.executors.TestCaseAfterCommandLineExecutor;
import com.mgm.cbmf.models.executors.TestCaseBeforeCommandLineExecutor;

/**
 * start all scenarios and his test cases. Measure the start and end time for
 * each test case.
 * 
 * @author bugge
 * @since 27.01.2016
 */
public class BenchmarkManager {

	private PropertiesElement properties;
	private BenchmarkTimesResultElement results = new BenchmarkTimesResultElement();
	private ComparatorManager comparator;
	private CleanupManager cleanup;
	private boolean isfirstRoundInScenario = false;
	private int roundNumberInScenario = 0; 
	private boolean logJarOutput = false;

	/**
	 * 
	 * @param properties -> contain all properties for this benchmark 
	 */
	public BenchmarkManager(PropertiesElement properties) {
		this.properties = properties;
		this.logJarOutput = properties.isLogJavaOutput();
	}
	
	/**
	 * start benchmark for all rounds for each test cases and each scenarios.
	 */
	public boolean run() {
		if (!this.properties.isValid()) {
			return true;
		}
		results = new BenchmarkTimesResultElement();
		for(String scenarioKey : new TreeSet<String>(this.properties.getScenarios().keySet())){
			ScenarioElement scenario = this.properties.getScenarios().get(scenarioKey);
			if (scenario.isValid(scenarioKey)) {
				results.addScenariosTimes(scenarioKey, runScenario(scenario));
			}
		}
		return false;
	}

	/**
	 * 
	 * @return the benchmark results
	 */
	public BenchmarkTimesResultElement getResults() {
		return this.results;
	}
	
	/**
	 * execute all jars from one scenario
	 * @param scenario -> properties from this scenario
	 * @return -> all results from this scenario
	 */
	private ScenarioTimesResultElement runScenario(ScenarioElement scenario) {
		this.isfirstRoundInScenario = true;
		this.roundNumberInScenario = 0;
		ScenarioTimesResultElement scenarioTimes = new ScenarioTimesResultElement();
		this.comparator = new ComparatorManager(scenario);
		this.cleanup = new CleanupManager(scenario);
		for(String testcaseKey: new TreeSet<String>(scenario.getTestCases().keySet())){
			TestCaseElement testcase = scenario.getTestCases().get(testcaseKey);
			if (testcase.isValid(testcaseKey)) {
				if(executeBeforMethodInTestCase(testcase)){
					continue;
				}
				scenarioTimes.setName(scenario.getName());
				scenarioTimes.addTestcaseTimes(testcaseKey, runTestCase(testcase, scenario));
				executeAfterMethodInTestCase(testcase);
			}
		}
		return scenarioTimes;
	}
	
	/**
	 * execute before method for selected test case 
	 * @param testcase -> properties for test case
	 * @return true if occur error
	 */
	private boolean executeBeforMethodInTestCase(TestCaseElement testcase){
		if(testcase.existBeforeCommand()){
			TestCaseBeforeCommandLineExecutor executor = new TestCaseBeforeCommandLineExecutor(testcase.getName(), testcase.getBeforeCommand());
			executor.setLogJarOutput(logJarOutput);
			return executor.run();
		}
		return false;
	}
	
	/**
	 * execute after method for selected test case 
	 * @param testcase -> properties for test case
	 * @return true if occur error
	 */
	private boolean executeAfterMethodInTestCase(TestCaseElement testcase){
		if(testcase.existAfterCommand()){
			TestCaseAfterCommandLineExecutor executor = new TestCaseAfterCommandLineExecutor(testcase.getName(), testcase.getAfterCommand());
			executor.setLogJarOutput(logJarOutput);
			return executor.run();
		}
		return false;
	}
	
	/**
	 * execute all jars from one test case 
	 * @param testcase -> are the properties from this test case
	 * @param scenario -> are the properties from the scenario. This test case is a part of scenario.
	 * @return the results from this test case.
	 */
	private TestCaseTimesResultElement runTestCase(TestCaseElement testcase,
			ScenarioElement scenario) {
		
		TestCaseTimesResultElement testcaseTimes = new TestCaseTimesResultElement();
		runGenerator(testcase,scenario,testcaseTimes);
		
		for (int i = 0; i < scenario.getRoundNumber(); i++) {
			testcaseTimes.setName(testcase.getName());
			testcaseTimes.addTime(runTestCaseRound(testcase, scenario, i));
			roundNumberInScenario++;
		}
		return testcaseTimes;
	}

	/**
	 * execute the jar for one round. this method measure the execute time from the jar and compare the results.
	 * @param testcase -> are the properties from the test case. This round is a part of test case.
	 * @param scenario -> are the properties from the scenario. The testcase is a part of scenario.
	 * @param round -> is the number of this round
	 * @return the results from this round 
	 */
	private TimeResultElement runTestCaseRound(TestCaseElement testcase, ScenarioElement scenario, int round) {
		TimeResultElement time = new TimeResultElement();
		if(testcase.existCommandLine()){
			time.setExecuteError(executeCommandLine(testcase, scenario, round,time));
		}else if(testcase.existJarPath()){
			time.setExecuteError(executeJar(testcase, scenario, round,time));
		}
		time.setComperatorEnable(false);
		runComparator(testcase,scenario,round,time);
		runCleanup(testcase,scenario,round,time);
		this.isfirstRoundInScenario=false;
		return time;
	}
	
	/**
	 * run the comparator
	 * @param testcase -> are the properties from the test case. This round is a part of test case.
	 * @param scenario -> are the properties from the scenario. The testcase is a part of scenario.
	 * @param round -> is the number of this round
	 * @param timeResult -> save the comparator status
	 */
	private void runComparator(TestCaseElement testcase, ScenarioElement scenario, int round, TimeResultElement timeResult){
		if(scenario.getCompareSettings().getEnable()){
			timeResult.setComparatorError(compareResults(testcase,round));
			timeResult.setComperatorEnable(true);
		}
	}
	
	private void runCleanup(TestCaseElement testcase, ScenarioElement scenario, int round, TimeResultElement timeResult) {
		if(scenario.getCleanupSettings().getEnable()){
			timeResult.setCleanupError(cleanupResults(scenario,testcase,round));
			timeResult.setCleanupEnable(true);
		}
	}
	
	private void runGenerator(TestCaseElement testcase, ScenarioElement scenario,TestCaseTimesResultElement testcaseTimes){
		if(scenario.getGeneratorSettings().getEnable()){
			GeneratorManager executor = new GeneratorManager(scenario);
			testcaseTimes.setGeneratorError(executor.generate(getSourcePath(scenario,testcase)));
			testcaseTimes.setGeneratorEnable(true);
		}else {
			testcaseTimes.setGeneratorEnable(false);
		}
	}

	/**
	 * execute the jar to measure for the round
	 * @param testcase -> is the test case for this round
	 * @param scenario -> is the scenario for this test case
	 * @param round -> is the number from this round
	 * @param timeResult -> to save the start and end time stamp
	 * @return true if occur a error
	 */
	private boolean executeJar(TestCaseElement testcase, ScenarioElement scenario, int round,TimeResultElement timeResult) {
		BenchmarkJarExecutor executor = new BenchmarkJarExecutor(testcase.getJarPath());
		executor.setScenarioName(scenario.getName());
		executor.setTestCaseName(testcase.getName());
		executor.setOptions(getOptionsForTestCaseJar(testcase));
		executor.setRound(round);
		executor.setLogJarOutput(logJarOutput);
		boolean status = executor.runTestCaseRound(getSourcePath(scenario,testcase),getTargetPath(testcase,round));
		timeResult.setStartTime(executor.getStartTimeStamp());
		timeResult.setEndTime(executor.getEndTimeStamp());
		return status;
	}
	
	/**
	 * execute the command to measure for the round
	 * @param testcase -> is the test case for this round
	 * @param scenario -> is the scenario for this test case
	 * @param round -> is the number from this round
	 * @param timeResult -> to save the start and end time stamp
	 * @return true if occur a error
	 */
	private boolean executeCommandLine(TestCaseElement testcase, ScenarioElement scenario, int round,TimeResultElement timeResult){
		BenchmarkCommandLineExecutor executor = new BenchmarkCommandLineExecutor(testcase.getCommandLine());
		executor.setScenarioName(scenario.getName());
		executor.setTestCaseName(testcase.getName());
		executor.setRound(round);
		executor.setLogJarOutput(logJarOutput);
		boolean status = executor.runTestCaseRound(getSourcePath(scenario,testcase),getTargetPath(testcase,round));
		timeResult.setStartTime(executor.getStartTimeStamp());
		timeResult.setEndTime(executor.getEndTimeStamp());
		return status;
	}
	
	/**
	 * get the target Path
	 * @param testcase -> are the test case properties
	 * @param round -> is the current round
	 * @return the path to save the results
	 */
	private String getTargetPath(TestCaseElement testcase,int round){
		if(testcase.getResultFolderPath().endsWith("/")){
			return testcase.getResultFolderPath()+Long.toString(properties.getStartDate().getTime())+"/"+Integer.toString(round);
		}
		return testcase.getResultFolderPath()+"/"+Long.toString(properties.getStartDate().getTime())+"/"+Integer.toString(round);
	}
	
	private String getSourcePath(ScenarioElement scenario,TestCaseElement testcase){
		if(scenario.getGeneratorSettings().getEnable()){
			if(scenario.getSourceFolderPath().endsWith("/")){
				return scenario.getSourceFolderPath()+testcase.getCaseIndicator()+"/"+Long.toString(properties.getStartDate().getTime());
			}
			return scenario.getSourceFolderPath()+"/"+testcase.getCaseIndicator()+"/"+Long.toString(properties.getStartDate().getTime());
		}
		return scenario.getSourceFolderPath();
	}
	
	/**
	 * 
	 * @param testcase -> is the test case to get the options
	 * @return the options for the test case jar
	 */
	private String getOptionsForTestCaseJar(TestCaseElement testcase){
		if(testcase.existJarOptions()){
			return testcase.getJarOptions();
		}
		return DefaultValues.testCaseOptions;
	}
	
	/**
	 * execute the comparator jar to compare the results from this round with the last round or reference results
	 * @param testcase -> are the test case properties
	 * @param round -> is the number of this round
	 * @return true is occur a error
	 */
	private boolean compareResults(TestCaseElement testcase,int round) {
		this.comparator.setResultTargetPath(getTargetPath(testcase,round));
		return this.comparator.compare(this.isfirstRoundInScenario);
	}
	
	private boolean cleanupResults(ScenarioElement scenario,TestCaseElement testcase,int round){
		this.cleanup.setCleanupPath(getTargetPath(testcase,round));
		return this.cleanup.cleanup(getSourcePath(scenario,testcase),
				this.isfirstRoundInScenario, isLastRoundInScenario(scenario),isLastRoundInTestCase(scenario,round));
	}
	
	private boolean isLastRoundInScenario(ScenarioElement scenario){
		if((this.roundNumberInScenario+1) >= maxRoundNumberFromScenario(scenario)){
			return true;
		}
		return false;
	}
	
	private boolean isLastRoundInTestCase(ScenarioElement scenario, int round){
		return (round+1) < scenario.getRoundNumber() ? false : true;
	}
	
	private int maxRoundNumberFromScenario(ScenarioElement scenario){
		return scenario.getRoundNumber()*scenario.getTestCases().size();
	}
}
