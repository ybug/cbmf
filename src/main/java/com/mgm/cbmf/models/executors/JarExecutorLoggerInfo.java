package com.mgm.cbmf.models.executors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import com.mgm.cbmf.models.StringTemplates;

/**
 * this class log the jar output (info)
 * 
 * @author bugge
 * @since 28.01.2016
 */
public class JarExecutorLoggerInfo implements Runnable {
	private final static Logger LOGGER = Logger.getLogger(JarExecutorLoggerInfo.class.getName());
	private BufferedReader reader;

	/**
	 * 
	 * @param is
	 *            -> input stream from the executable jar
	 */
	public JarExecutorLoggerInfo(InputStream is) {
		this.reader = new BufferedReader(new InputStreamReader(is));
	}

	/**
	 * write the input stream in the log
	 */
	public void run() {
		try {
			String line = reader.readLine();
			while (line != null) {
				LOGGER.info(StringTemplates.outputJarExecutorLogger(line));
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
