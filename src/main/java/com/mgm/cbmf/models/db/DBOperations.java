package com.mgm.cbmf.models.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.mgm.cbmf.models.StringTemplates;

public class DBOperations {

	private String jdbcDriver = "com.mysql.jdbc.Driver";
	private final String dbUrl;
	private final String user;
	private final String password;

	private Connection conn = null;
	private Statement stmt = null;

	private final static Logger LOGGER = Logger.getLogger(DBOperations.class.getName());

	public DBOperations(String dbUrl, String user, String password) {
		this.dbUrl = dbUrl;
		this.user = user;
		this.password = password;
	}

	/**
	 * open the connection to the Database
	 * 
	 * @return true if occur a error
	 */
	public boolean open() {
		try {
			Class.forName(jdbcDriver);
			conn = DriverManager.getConnection(dbUrl, user, password);
			stmt = conn.createStatement();
		} catch (SQLException | ClassNotFoundException e) {
			LOGGER.error(StringTemplates.outputDBConnectionError(), e);
			close();
			return true;
		}
		return false;
	}
	
	/**
	 * execute SQL query
	 * @param sql
	 * @return if occur error than return null, else ResultSet
	 */
	protected ResultSet execute(String sql){
		try {
			return stmt.executeQuery(sql);
		} catch (SQLException e) {
			LOGGER.error(StringTemplates.outputDBReadError(), e);
			return null;
		}
	}
	
	protected void closeResultSet(ResultSet resultSet){
		if(resultSet != null){
			try {
				resultSet.close();
			} catch (SQLException e) {
			}
		}
	}
	
	public void close() {
		try {
			if (stmt != null) {
				stmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {

		}
	}

	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}
}
