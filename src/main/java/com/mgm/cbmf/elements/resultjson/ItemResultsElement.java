package com.mgm.cbmf.elements.resultjson;

import java.util.ArrayList;
import java.util.List;

/**
 * contain the results from one item 
 * @author bugge
 * @since 04.02.2016
 */
public class ItemResultsElement {
	private Integer id;
	private List<ItemResultElement> results = new ArrayList<ItemResultElement>();
	private String json;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getJson() {
		return json;
	}
	public void setJson(String json) {
		this.json = json;
	}
	public List<ItemResultElement> getResults() {
		return results;
	}
	public void setResults(List<ItemResultElement> results) {
		this.results = results;
	}
	@Override
	public String toString() {
		return "ItemResultsElement |"+results.size()+"|[id=" + id + ", results=" + results + ", json=" + json + "]";
	}
}
