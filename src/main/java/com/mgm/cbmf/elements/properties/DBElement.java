package com.mgm.cbmf.elements.properties;

public class DBElement {

	private boolean enable = false;
	private String jdbcDriver = null;
	private String dbUrl = null;
	private String user = null;
	private String password = null;
	
	public boolean isValide(){
		if(dbUrl == null){
			return false;
		}else if(user == null){
			return false;
		}else if(password == null){
			return false;
		}else{
			return true;
		}
	}
	
	public boolean existJdbcDriver(){
		return (jdbcDriver == null) ? false : true;
	}
	
	public String getJdbcDriver() {
		return jdbcDriver;
	}
	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}
	public String getDbUrl() {
		return dbUrl;
	}
	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}
}
