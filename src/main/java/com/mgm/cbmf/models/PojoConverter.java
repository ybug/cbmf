package com.mgm.cbmf.models;

/**
 * convert pojo 
 * @author bugge
 * @since 05.05.2016
 */
public class PojoConverter {
	
	/**
	 * check the String -> is content a long value
	 * @param value -> is a string
	 * @return true if contain a long value
	 */
	public static boolean isLong(String value){
		if(stringToLong(value) == null){
			return false;
		}
		return true;
	}
	
	/** Convert String to Long
	 *  @param value is the string to convert
	 *  @return is the null if occur a error or the long value
	 */ 
	public static Long stringToLong(String value){
		try{
			return Long.parseLong(value);
		}catch(NumberFormatException e){
			return null;
		}
	}
	
	/**
	 * check the String -> is content a double value
	 * @param value -> is the String to check 
	 * @return true if contain a double value
	 */
	public static boolean isDouble(String value){
		if(stringToDouble(value) == null){
			return false;
		}
		return true;
	}
	
	/** Convert String to Double
	 *  @param value is the string to convert
	 *  @return is the null if occur a error or the double value
	 */ 
	public static Double stringToDouble(String value){
		try{
			return Double.parseDouble(value);
		}catch(NumberFormatException e){
			return null;
		}
	}
	
}
