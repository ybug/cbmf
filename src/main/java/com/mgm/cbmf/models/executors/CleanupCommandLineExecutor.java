package com.mgm.cbmf.models.executors;

import com.mgm.cbmf.models.DefaultValues;
import com.mgm.cbmf.models.StringTemplates;

public class CleanupCommandLineExecutor extends CommandLineExecutor{

	private String command;
	private String cleanUpFolderPath;
	private boolean logJarOutput = true;
	
	private final boolean ERROR = true;
	private final boolean SUCCESFUL= !ERROR;
	
	
	public CleanupCommandLineExecutor(String command) {
		this.command = command;
	}
	
	@Override
	public String getCommand() {
		return command.replaceAll(DefaultValues.cleanUpFolderPathPlaceholders, cleanUpFolderPath);
	}

	@Override
	public String getErrorMessage() {
		return StringTemplates.outputExecutorCleanUpEndTitle(cleanUpFolderPath, ERROR);
	}

	@Override
	public String getInfoMessage() {
		return StringTemplates.outputExecutorCleanUpEndTitle(cleanUpFolderPath, SUCCESFUL);
	}

	@Override
	public boolean isLogExecuteOutput() {
		return logJarOutput;
	}
	
	public void setLogJarOutput(boolean logJarOutput) {
		this.logJarOutput = logJarOutput;
	}

	public void setCleanUpFolderPath(String cleanUpFolderPath) {
		this.cleanUpFolderPath = cleanUpFolderPath;
	}
}
