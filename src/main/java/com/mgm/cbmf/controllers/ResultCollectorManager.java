package com.mgm.cbmf.controllers;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mgm.cbmf.elements.benchmark.BenchmarkTimesResultElement;
import com.mgm.cbmf.elements.benchmark.ScenarioTimesResultElement;
import com.mgm.cbmf.elements.benchmark.TestCaseTimesResultElement;
import com.mgm.cbmf.elements.benchmark.TimeResultElement;
import com.mgm.cbmf.elements.properties.PropertiesElement;
import com.mgm.cbmf.elements.properties.ScenarioDefaultValue;
import com.mgm.cbmf.elements.properties.ScenarioElement;
import com.mgm.cbmf.elements.resultjson.BenchmarkResultsElement;
import com.mgm.cbmf.elements.resultjson.HostInformationElement;
import com.mgm.cbmf.elements.resultjson.HostResultElement;
import com.mgm.cbmf.elements.resultjson.ItemInformationElement;
import com.mgm.cbmf.elements.resultjson.ItemResultsElement;
import com.mgm.cbmf.elements.resultjson.RoundResultElement;
import com.mgm.cbmf.elements.resultjson.ScenarioDefaultValueResultElement;
import com.mgm.cbmf.elements.resultjson.ScenarioResultElement;
import com.mgm.cbmf.elements.resultjson.TestCaseResultElement;
import com.mgm.cbmf.models.PojoConverter;
import com.mgm.cbmf.models.StringTemplates;
import com.mgm.cbmf.models.ZabbixAPI;
import com.mgm.cbmf.models.db.ZabbixDBHistoryTableOperations;

/**
 * collect the results from zabbix
 * 
 * @author bugge
 * @since 04.02.2016
 */
public class ResultCollectorManager {

	private BenchmarkTimesResultElement timeResults;
	private BenchmarkResultsElement results = new BenchmarkResultsElement();
	private ZabbixAPI zabbixApi;
	private PropertiesElement properties;
	private ZabbixDBHistoryTableOperations zabbixDB = null;
	private boolean connect = true;

	private final static Logger LOGGER = Logger.getLogger(ResultCollectorManager.class.getName()); 

	/**
	 * 
	 * @param properties -> are the properties for this benchmark
	 * @param timeResults -> are the results from the benchmark without the zabbix informations 
	 */
	public ResultCollectorManager(PropertiesElement properties, BenchmarkTimesResultElement timeResults) {
		this.timeResults = timeResults;
		this.properties = properties;
		
		if(properties.getDb().isEnable() && properties.getDb().isValide()){
			zabbixDB = new ZabbixDBHistoryTableOperations(properties.getDb().getDbUrl(),properties.getDb().getUser(), properties.getDb().getPassword());
			if(properties.getDb().existJdbcDriver()){
				zabbixDB.setJdbcDriver(properties.getDb().getJdbcDriver());
			}
		}
		
		this.zabbixApi = new ZabbixAPI(properties.getZabbixProperties());
		if (!zabbixApi.isConnectedWithLogOutput()) {
			zabbixApi.close();
			this.connect = false;
		}
		this.results.setZabbixWasConnected(this.connect);
	}

	/**
	 * collect the zabbix data
	 */
	public void collect() {
		results = new BenchmarkResultsElement();
		setInBenchmarkResultsTimes();
		setInScenarioResultDefaultValues();
		if (connect) {
			collectZabbixMatadata();
			collectZabbixItems();
		}
	}
	
	/**
	 * logout and close the connections zabbix.
	 */
	public void closeZabbix() {
		if(zabbixDB != null){
			zabbixDB.close();
		}
		zabbixApi.close();
		this.connect = false;
	}
	
	/**
	 * 
	 * @return get the results with zabbix informations
	 */
	public BenchmarkResultsElement getResults() {
		return results;
	}
	
	/**
	 * Convert the BenchmarkTimesResultElement to BenchmarkResultsElement. 
	 */
	private void setInBenchmarkResultsTimes() {
		results.setBenchmarkName(properties.getBenchmarkName());
		for (String scenarioName : timeResults.getScenariosTimes().keySet()) {
			ScenarioResultElement scenarioResultElement = new ScenarioResultElement();
			ScenarioTimesResultElement scenarioTimeElement = timeResults.getScenariosTimes().get(scenarioName);
			scenarioResultElement.setName(scenarioTimeElement.getName());
			for (String testCaseName : scenarioTimeElement.getTestcasesTimes().keySet()) {
				TestCaseResultElement testCaseResultElement = new TestCaseResultElement();
				TestCaseTimesResultElement testCase = scenarioTimeElement.getTestcasesTimes().get(testCaseName);
				testCaseResultElement.setGeneratorEnable(testCase.getGeneratorEnable());
				testCaseResultElement.setGeneratorError(testCase.getGeneratorError());
				testCaseResultElement.setName(testCase.getName());
				for (TimeResultElement time : testCase.getTimes()) {
					RoundResultElement roundResult = new RoundResultElement();
					roundResult.setCleanupEnable(time.getCleanupEnable());
					roundResult.setCleanupError(time.getCleanupError());
					roundResult.setStartTime(time.getStartTime());
					roundResult.setEndTime(time.getEndTime());
					roundResult.setExecuteError(time.isExecuteError());
					roundResult.setComparatorError(time.getComparatorError());
					roundResult.setComparatorEnable(time.getComperatorEnable());
					testCaseResultElement.getRounds().add(roundResult);
				}
				scenarioResultElement.getTestCases().put(testCaseName, testCaseResultElement);
			}
			results.getScenarios().put(scenarioName, scenarioResultElement);

		}
	}
	
	/**
	 * set in scenario result elements all default values from the properties file
	 */
	private void setInScenarioResultDefaultValues(){
		for(String scenarioName : properties.getScenarios().keySet()){
			ScenarioElement scenario = properties.getScenarios().get(scenarioName);
			for(String defaultValueID : scenario.getDefaultValues().keySet()){
				ScenarioDefaultValue propertiesDefaultValue = scenario.getDefaultValues().get(defaultValueID);
				if(!propertiesDefaultValue.isValid()){
					LOGGER.warn(StringTemplates.outputDefaultValueNotValid(defaultValueID));
					continue;
				}
				ScenarioDefaultValueResultElement resultDefaultValue = new ScenarioDefaultValueResultElement();
				resultDefaultValue.setId(propertiesDefaultValue.getId());
				resultDefaultValue.setName(propertiesDefaultValue.getName());
				resultDefaultValue.setUnit(propertiesDefaultValue.getUnit());
				String stringValue = propertiesDefaultValue.getValue();
				if(PojoConverter.isLong(stringValue)){
					resultDefaultValue.setValueLong(PojoConverter.stringToLong(stringValue));
				} else if(PojoConverter.isDouble(stringValue)){
					resultDefaultValue.setValueDouble(PojoConverter.stringToDouble(stringValue));
				}else {
					resultDefaultValue.setValueString(stringValue);
				}
				results.getScenarios().get(scenarioName).getDefaultValues().put(defaultValueID, resultDefaultValue);
			}
		}
		
	}
	
	/**
	 * collect and save in BenchmarkResultsElement the meta data like host group, hosts and used items informations.  
	 */
	private void collectZabbixMatadata() {
		results.setGroupInformation(this.zabbixApi.getHostGroupInformationElement());
		if (results.getGroupInformation().getId() >= 0) {
			results.setHostsInformation(this.zabbixApi.getHostsByGroupID(results.getGroupInformation()));
			this.zabbixApi.setInBenchmarkResultElementItemsInformations(results);
		}
		results.setZabbixWasConnected(true);
	}

	/**
	 * collect and save in BenchmarkResultsElement the results from zabbix. 
	 */
	private void collectZabbixItems() {
		for(String scenarioName : results.getScenarios().keySet()){
			ScenarioResultElement scenario = results.getScenarios().get(scenarioName);
			for(String testCaseName: scenario.getTestCases().keySet()){
				TestCaseResultElement testCase = scenario.getTestCases().get(testCaseName);
				for(RoundResultElement round : testCase.getRounds()){
					if (!round.getExecuteError()) {
						round.setItemsByHost(getTimeResultsFromHostsByRound(round));
					}
				}
			}
		}
	}

	/**
	 * set for one round all items from all hosts
	 * 
	 * @param round
	 *            -> information about a round
	 * @return all items results from all hosts
	 */
	private Map<Integer, HostResultElement> getTimeResultsFromHostsByRound(RoundResultElement round) {
		Map<Integer, HostResultElement> itemsByHost = new HashMap<Integer,HostResultElement>();
		for (Integer hostID : results.getHostsInformation().keySet()) {
			HostInformationElement host = results.getHostsInformation().get(hostID);
			if (host.isEnable()) {
				HostResultElement hostResults = new HostResultElement();
				hostResults.setId(hostID);
				hostResults.setItems(getItemsResults(round, host));
				itemsByHost.put(hostID, hostResults);
			}
		}
		return itemsByHost;
	}
	
	/**
	 * get from one round all items from one host
	 * @param round -> information about the round like start and end time stamp
	 * @param host -> information about the host, like host ID 
	 * @return -> map with item id as key and item results as value. The items are not empty 
	 */
	private Map<Integer, ItemResultsElement> getItemsResults(RoundResultElement round, HostInformationElement host) {
		Map<Integer, ItemResultsElement> itemsResults = new HashMap<Integer, ItemResultsElement>();
		for (Integer itemID : host.getItemsID()) {
			ItemInformationElement item = results.getItemsInformation().get(itemID);
			if (item.isEnable()) {
				
				ItemResultsElement itemResult = getItemResultsElement(round,host,item);
				if(itemResult != null){
					itemsResults.put(itemID, itemResult);
				}
				
			}
		}
		return itemsResults;
	}
	
	private ItemResultsElement getItemResultsElement(RoundResultElement round, HostInformationElement host,ItemInformationElement item){

		if(properties.getDb().isEnable() && properties.getDb().isValide()){
			return getItemResultsElementFromZabbixDB(round,host,item);
		}else{
			return getItemResultsElementFromZabbixAPI(round,host,item);
		}
	}
	
	private ItemResultsElement getItemResultsElementFromZabbixAPI(RoundResultElement round, HostInformationElement host,ItemInformationElement item){
		ItemResultsElement itemResult = zabbixApi.getItemResultsElementWithZabbixResults(round, host, item);
		if(!zabbixApi.isError()&& !itemResult.getResults().isEmpty()){
			return itemResult;
		}else{
			return null;
		}
	}
	
	private ItemResultsElement getItemResultsElementFromZabbixDB(RoundResultElement round, HostInformationElement host,ItemInformationElement item){
		
		Timestamp start = round.getStartTime();
		Timestamp end = round.getEndTime();
		Integer itemID = item.getId();
		Integer history = item.getDataformat();
		zabbixDB.readHistory(history, itemID, start.getTime()/1000L, end.getTime() / 1000L);
		return zabbixDB.getResults();
	}
}
