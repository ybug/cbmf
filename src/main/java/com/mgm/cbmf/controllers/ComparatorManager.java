package com.mgm.cbmf.controllers;

import com.mgm.cbmf.elements.properties.ScenarioElement;
import com.mgm.cbmf.models.DefaultValues;
import com.mgm.cbmf.models.executors.ComparatorCommandLineExecutor;
import com.mgm.cbmf.models.executors.ComparatorJarExecutor;

/**
 * This class execute the jars to compare the results from the different test
 * cases and rounds. The results compare with the last round or with reference
 * results.
 * 
 * @author bugge
 * @since 24.04.2016
 */
public class ComparatorManager {

	private ScenarioElement properties;
	private String referenceResultTargetPath = null;
	private String currentResultTargetPath = null;

	/**
	 * 
	 * @param properties
	 *            -> is the properties from one scenario
	 */
	public ComparatorManager(ScenarioElement properties) {
		this.properties = properties;
	}

	/**
	 * set the resultTargetPath and compute the last result Target Path
	 * 
	 * @param resultTargetPath
	 */
	public void setResultTargetPath(String resultTargetPath) {
		if (properties.getCompareSettings().existReferenceDatasetPath()) {
			referenceResultTargetPath = properties.getCompareSettings().getReferenceDatasetPath();
			currentResultTargetPath = resultTargetPath;
		} else {
			referenceResultTargetPath = currentResultTargetPath;
			currentResultTargetPath = resultTargetPath;
		}
	}

	/**
	 * compare the result from this run either with the referencePath from the
	 * property or from the last round
	 * 
	 * @return if occur a error, then return true
	 */
	public boolean compare(boolean firstRoundInScenario) {
		if (!existReferenceTargetPath()) {
			return false;
		}
		if (properties.getCompareSettings().existCommandLine()) {
			return executeInCommandLine(firstRoundInScenario);
		} else {
			return executeInJar(firstRoundInScenario);
		}
	}
	
	public boolean executeInCommandLine(boolean firstRoundInScenario){
		ComparatorCommandLineExecutor executer = new ComparatorCommandLineExecutor(
				properties.getCompareSettings().getCommandLine());
		executer.setLogJarOutput(properties.getCompareSettings().getLogJarOutput());
		executer.setFirstRoundInScenario(firstRoundInScenario);
		executer.setReferenceResultsPath(referenceResultTargetPath);
		executer.setReportPath(getComparatorReportPath());
		executer.setTargetResultsPath(currentResultTargetPath);
		return executer.run();
	}
	
	public boolean executeInJar(boolean firstRoundInScenario){
		ComparatorJarExecutor executer = new ComparatorJarExecutor(properties.getCompareSettings().getJarPath());
		executer.setLogJarOutput(properties.getCompareSettings().getLogJarOutput());
		executer.setOptions(getComparatorJarOptions());
		return executer.runComparator(referenceResultTargetPath, currentResultTargetPath, firstRoundInScenario,
				getComparatorReportPath());
	}

	/**
	 * 
	 * @return the report path for the comparator report
	 */
	private String getComparatorReportPath() {
		if (properties.getCompareSettings().existReportPath()) {
			return properties.getCompareSettings().getReportPath();
		}
		return DefaultValues.comparatorOptions;
	}

	/**
	 * 
	 * @return the options for the comparator jar
	 */
	private String getComparatorJarOptions() {
		if (properties.getCompareSettings().existJarOptions()) {
			return properties.getCompareSettings().getJarOptions();
		}
		return "";
	}

	/**
	 * 
	 * @return true if exist reference result path.
	 */
	private boolean existReferenceTargetPath() {
		if (referenceResultTargetPath != null) {
			return true;
		}
		return false;
	}
}
