package com.mgm.cbmf.models.executors;

import com.mgm.cbmf.models.DefaultValues;
import com.mgm.cbmf.models.StringTemplates;

public class GeneratorCommandLineExecutor extends CommandLineExecutor{
	
	private String command;
	private String generatedSourcePath;
	private boolean logJarOutput = true;
	
	private final boolean ERROR = true;
	private final boolean SUCCESFUL= !ERROR;
	
	public GeneratorCommandLineExecutor(String command) {
		this.command = command;
	}
	
	public void setLogJarOutput(boolean logJarOutput) {
		this.logJarOutput = logJarOutput;
	}

	@Override
	public String getCommand() {
		return command.replaceAll(DefaultValues.generateSourcePathPlaceholders, generatedSourcePath);
	}

	@Override
	public String getErrorMessage() {
		return StringTemplates.outputExecutorGeneratorEndTitle(this.generatedSourcePath,ERROR);
	}

	@Override
	public String getInfoMessage() {
		return StringTemplates.outputExecutorGeneratorEndTitle(this.generatedSourcePath,SUCCESFUL);
	}

	@Override
	public boolean isLogExecuteOutput() {
		return logJarOutput;
	}

	public void setGeneratedSourcePath(String generatedSourcePath) {
		this.generatedSourcePath = generatedSourcePath;
	}
}
